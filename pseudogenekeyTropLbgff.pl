#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my @line;
my @id;
my %exonstart;
my %exonstop;
my %exon;
my @list;
my $list;
my %pacid;
my %query;
my %start;
my %stop;
my @blastdbcmdnt; #blast data for blastdbcmd on genome for nucleotides of pseudogene
my @blastdbcmdprot; #blast data for blastdbcmd on proteome for protein sequence for paralog of pseudogene
my $i=1;
my $v=1;
my @exonerate;

open (IN, "Key") || die "nope\n";
while ($list = <IN>) { 		#reads in the filed with pseudogenes, sorted and added 500 bp on either side / on one scaffold. 
	@list=split(/\t/, $list);
	
	$pacid{$i}=$list[0];
	$query{$i}=$list[1];
	$start{$i}=$list[4];
	$stop{$i}=$list[5];
	$i++;
}
  
#    chomp;
#   push (@list, "$_"); 		#every line gets stored in an array


close (IN);
while ($v < $i)  { 		#for each line in the array
 #   $pacid = $list[0]; 		#the first column is the pacid that the pseudogene is expected to align most closely to
  #  $query = $list[1]; 		#the second column is the Scaffold on which that pseudogene can be found
   # $start = $list[4]; 		#the location 500bp before where the pseudogene begins aligning to its paralog
   # $stop = $list[5];  		#the location 500bp after where the pseudogene ends aligning to its paralog
	
#	print ("$pacid $query $start");
	print ("$pacid{$v} $query{$v} $start{$v} $stop{$v} ");
  # ($query, $start, $stop) = split(/\t/, $v);
  # push (@blastdbcmd, `blastdbcmd -dbtype nucl -db /house/homedirs/a/amsession/Xenopus/full_repeats.fa -entry $query`);

	push (@blastdbcmdnt, `blastdbcmd -db ~/Laevis/Genome/LAEVIS_6.1.repeatMasked.fa -dbtype nucl -entry $query{$v} -range $start{$v}-$stop{$v} -out "./middle2/$v.nt"`);
	#runs blastdcmd on Laevis genome finding location 500 bp +- where pseudogene is found
	
	push (@blastdbcmdprot, `blastdbcmd -db ~/Sahar_work/New_data_030613/Xtr_72_longest_pep.fa -dbtype prot -entry 'lcl|$pacid{$v}' -out "./middle2/$v.prot"`);
	#runs blastdcmd on Tropicalis proteome finding protein sequence for the paralog of pseudogene

	push (@exonerate, `exonerate --exhaustive y --showalignment False --showvulgar False --showtargetgff True -S False -m protein2genome:bestfit ./middle/$v.prot ./middle2/$v.nt >> ./output/pseudogffs`);
	# --ryo ">%ti (%tcb - %tce)\n%tcs\n"   
	
	# to get the dna sequence that aligns to protein w/o splice sites or introns or frameshifts. 
	
	#CHANGED: to show vulgar output to parse for insertions/deletions
	$v++;
}

foreach my $i (@blastdbcmdnt) {
    print "$i\n";
}
