#!/usr/bin/env perl

use strict;
use warnings;
my $line;
my @line;
my @files;
my $file;

#open (ONE, "fastamuscle") or die "Cannot open: $! \n";
#open (OUT, ">fastaoutput") or die "nope\n";

opendir(DH, "./middle3") or die "Cannot open: $! \n";
while ($file=readdir DH) {
	open (FILEH, "./middle3/$file") or die "nope: $! \n";
	open (OUT, ">./fasta4muscle/$file.fasta") or die "nope: $! \n";

    # skip . and ..
#    next if($file =~ /^\.$/);
 #   next if($file =~ /^\.\.$/);

    # $file is the file used on this iteration of the loop
	while ($line = <FILEH>) { #read one line at a time
		$line =~ /Command/ && next; #if it has a # go on to next line (common with gff files)
		$line =~ /%tcs/ && next;
		$line =~ / False/ && next;
		$line =~ /Hostname/ && next;
		$line =~ /completed/ &&next;
		$line =~ s/\:/\|/;
	#	@line=split(/\t/, $line);
		print OUT ($line);
	}
	close (FILEH);
	close (OUT);
}

closedir (DH);