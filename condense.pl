#!/usr/bin/env perl

use strict;
use warnings;

my @list;
my $list;
my %name;
my $j;

open (NAME, "./New_data_030613/Besthit.info") || die "nope\n";
open (ERROR, ">Errors") or die "$! \n";
while ($list = <NAME>) { 		 
	@list=split(/\t/, $list);
	my $i = $list[0];
	chomp $list[1];
	if ($list[1]) {
		$name{$i}=$list[1];
	} else {
		print ERROR ("No PacID on $i\n");
	}
}
close (NAME);
	
open (INFO, "./New_data_030613/nogapXB.candidates") || die "nope\n";
open (KEY, ">Key") or die "$! \n";
while ($list = <INFO>) { 		 
	@list=split(/\t/, $list);
	$j = $list[0];
	if (($name{$j})=~ m/\d{8}/){
		print KEY (join "\t", ($name{$j}), $list[1], $list[2], $list[3],$list[4], $list[5], $list[6], $list[7], $list[8]);
	} else {
		print ERROR (join "\t", ($name{$j}), $list[1], $list[2], $list[3],$list[4], $list[5], $list[6], $list[7], $list[8]);
	}
}

close (KEY);
close (INFO);
close (ERROR);