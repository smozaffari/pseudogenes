#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my @line;
my @files;
my $file;
my @name;
my $name;
my @phylip;

opendir(DH, "./fasta4muscle") or die "nope1: $! \n";
while ($file=readdir DH) {
	open (FILEM, "./fasta4muscle/$file") or die "can't open files in fasta4muscle: $! \n";
	
	my @name = split(/\./, $file);

	$name = $name[0];
	print $name;
	open (ONE, "> ./musclefastas/$name.muscle") or die "nope3: $! \n";
	(`muscle -in ./fasta4muscle/$file -out ./musclefastas/$name.muscle`);
	close (ONE);
	open (TWO, ">./phylipfiles/$name.ph") or die "nope2: $! \n";
	(`perl fasta_to_phylip.pl ./musclefastas/$name.muscle ./phylipfiles/$name.ph`);
	close (TWO);
	close (FILEM);

}

closedir (DH);