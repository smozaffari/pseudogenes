open (IN, "$ARGV[0]") || die "nope\n";
my @Align_info;
$count = 0;
while (<IN>)
{
    next if 1 .. 12;
#    chomp;                                                                                                                         
    my $Info = substr $_, 8, 62;
    unless ($Info =~  m/exonerate analysis/)
    {
        $count++;
        if ($count == 1)
        {
            $Align_info[0] .= $Info;
            next;
        }
        if ($count == 2)
        {
            $Align_info[1] .= $Info;
            next;
        }
        if ($count == 3)
        {
            $Align_info[2] .= $Info;
            next;
        }
        if ($count == 4)
        {
            $Align_info[3] .= $Info;
            next;
        }
        if ($count == 5)
        {
            $count = 0;
            next;
        }
    }
}
close IN;
print "$Align_info[0]$Align_info[3]";