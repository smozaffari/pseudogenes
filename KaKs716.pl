#!/usr/bin/env perl

use strict;
use warnings;

my $laevis;
my @sort3;
my $line;
my @line;
my $file;
my $name;
my $count=0;
my @save;
my @value;
my %nams;
my $Ka1;
my $Ka2;
my $Ka3;
my %size;
my %pseudo;
my %age;
my %Laevis;
my $temptrop;
my $temp;

open (FILE, ">./sample7.1") || die "nope7: $! \n"; 

open (AGE, "Age7.1") || die "nope6 : $! \n";
while (my $list = <AGE>) {
    chomp $list;
    my @list = split (/\t/, $list);
    if ($list[0] =~ /(\d{8})/) {
#        print FILE ("$1 \t");    
	$age{$1} = ($list[1]);
#print ("$1\t$age{$1}\t");
    }
}
close (AGE);
open (SIZE, "size") || die "nope4: $! \n";
while (my $list = <SIZE>) {
    if ($list =~ /\w/) {
	my @list = split (/\t/, $list);
	if ($list[1]=~ /\|/) {
	    my @sort = split (/\|/, $list[1]);
	    chomp $list[2];
	    print ("$sort[1] $list[2]\n");
	    $size{$sort[1]}=$list[2];
	}
    }
}
close (SIZE);

open (SAVE, "save") || die "nope5: $! \n";
while (my $list = <SAVE>) {
    if ($list =~/\w/) {
	chomp $list;
	my @list = split (/\t/, $list);
#	my @sort = split (/\|/, $list[1]); 
	if ($list[1] =~ /(\d{8})/) {
	    $name = $1;                #name = trop pacid  
	}
#	if ($sort[1] =~ /(\d{8})/) {
#	    $Laevis{$1} = ($name); #key = Laevispacid --> trop pacid in hash
#	    print ("$Laevis{$1}\t$name\t$1\n");
#	}
	chomp $name;
#my @sort2 = split (/\|/, $list[1]);
	if ($list[2]) {
	    if ($list[2] =~ /\|/) {
		@sort3 = split (/\|/, $list[2]);
		chomp $sort3[1];
		$pseudo{$name} = ($sort3[1]);
#    my $temp = ($sort2[1]);
	    } else {
		if ($list[2] =~ /\>/) {
		    @sort3=split(/>/, $list[2]);
		    $pseudo{$name}=$sort3[1];
		} else {
		    $pseudo{$name}=$list[2];
		}
		print ("$name$pseudo{$name}\n"); #name = trop, pseudo{trop} = pseudo
	    }
	}
    }
}

close (SAVE);

opendir (DH, "./dialignmask/Rv") or die "Cannot open: $! \n";
while ($file=readdir DH) {
    open (FILEH, "./dialignmask/Rv/$file") or die "nope2: $! \n";
    if ($file =~ /(\d{8})/) {            # saves name of file/Lpacid as $name
	print FILE ("$1\t\t");
	$name=($1);               #name = tropicalis pacid
	print ("$1\t$name\n");   
    } else {
	print ($file);
    }

    #print file ("$name \t");
    while ($line = <FILEH>) {
	@line=split(/\ /, $line);
	chomp $line[1];
	$save[$count] = (($line[1]));
	print FILE ("$save[$count]\t");
	if ($count==11) {
	    if ($size{$name}) {
		print FILE ("$size{$name}\t");
		#print ("$name\t$size{$name}\n");
	    }
	     
	    if ($pseudo{$name}) {
		$temp = $pseudo{$name};
		print ("pseudo: $pseudo{$name}\t $size{$name}\t$size{$temp}\n");
		print FILE ("$size{$temp}\t");
	    } else {
		print FILE ("NA\t");
	    }
	    if ($age{$name}) {
		print FILE ("$age{$name}\n");
	    } else {
		print FILE ("\n");
	    }
	}
	$count++;
    }
    $count=0;
    close (FILEH);

}

close (FILE);
closedir (DH);
