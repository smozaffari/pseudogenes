#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my @line;
my @id;
my %exonstart;
my %exonstop;
my %exon;
my @list;
my $list;
my %pacid;
my %query;
my %start;
my %stop;
my @blastdbcmdnt; #blast data for blastdbcmd on genome for nucleotides of pseudogene
my @blastdbcmdprot; #blast data for blastdbcmd on proteome for protein sequence for paralog of pseudogene
my @blastdbcmdLa; #laevis cdna
my $i=1;
my $v=1;
my @exonerate;
my %Laevisname;
my %Laeviscds;
my @trimest;
my $file2;

open (IN, "Tropinfo.candidates") || die "nope\n";
while ($list = <IN>) { 		#reads in the filed with pseudogenes, sorted and added 500 bp on either side / on one scaffold. 
	@list=split(/\t/, $list);
	$pacid{$i}=$list[0];
	$query{$i}=$list[1];
	$start{$i}=$list[4];
	$stop{$i}=$list[5];
	$i++;
}

open (FILE, "Besthit.triplet") || die "nope\n";
	while ($line = <FILE>) { 
		@line=split(/\t/, $line);
		chomp $line[1];
		my @two = split(/\|/, $line[0]);
		$Laevisname{$line[1]} = join(':', $two[0], $two[1]); 
		print ("$Laevisname{$line[1]}\t");
	}
#    chomp;
#   push (@list, "$_"); 		#every line gets stored in an array
close (FILE);


close (IN);
while ($v < $i)  { 		#for each line in the array
 #   $pacid = $list[0]; 		#the first column is the pacid that the pseudogene is expected to align most closely to
  #  $query = $list[1]; 		#the second column is the Scaffold on which that pseudogene can be found
   # $start = $list[4]; 		#the location 500bp before where the pseudogene begins aligning to its paralog
   # $stop = $list[5];  		#the location 500bp after where the pseudogene ends aligning to its paralog
	
#	print ("$pacid $query $start");
	print ("$pacid{$v} $query{$v} $start{$v} $stop{$v} ");

	
	my $name = $pacid{$v};
	print ("$name\t $Laevisname{$name}\n");
  # ($query, $start, $stop) = split(/\t/, $v);
    
    push (@blastdbcmdLa, `blastdbcmd -db ~/Sahar_work/L_CDS.fa -dbtype nucl -entry "$Laevisname{$name}" -out "./cdsproteins/$name.cds"`);
# get cds for #Laevis protein

	
	push (@blastdbcmdprot, `blastdbcmd -db ~/Sahar_work/New_data_030613/Xtr_72_longest_CDS.fa -dbtype nucl -entry 'lcl|$pacid{$v}' >> "./cdsproteins/$name.cds"`);
#runs blastdcmd on Tropicalis proteome finding cds for the ortholog of pseudogene in tropicalis

	push (@exonerate, `exonerate --exhaustive y --showalignment False --showvulgar False --ryo ">%ti (%tcb - %tce)\n%tcs\n" -S False -m protein2genome:bestfit ./newest3/$name.prot ./newest3/$name.nt > "./pseudocds/$name"`);
	# --ryo ">%ti (%tcb - %tce)\n%tcs\n"   sequence of coding
	
	# to get the dna sequence that aligns to protein w/o splice sites or introns or frameshifts. 
	
	#--ryo ">%ti (%tab - %tae)\n%tas\n" #forgot what this was for.
	$v++;
}

foreach my $i (@blastdbcmdnt) {
    print "$i\n";
}
