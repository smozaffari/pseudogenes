#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my @line;
my @id;
my %exonstart;
my %exonstop;
my %exon;
my @list;
my $list;
my %pacid;
my %query;
my %start;
my %stop;
my @blastdbcmdnt; #blast data for blastdbcmd on genome for nucleotides of pseudogene
my @blastdbcmdprot; #blast data for blastdbcmd on proteome for protein sequence for paralog of pseudogene
my @blastdbcmdLa; #laevis cdna
my $i=1;
my $v=1;
my @exonerate;
my %Laevisname;
my %Laeviscds;

open (IN, "Key") || die "nope\n";
while ($list = <IN>) { 		#reads in the filed with pseudogenes, sorted and added 500 bp on either side / on one scaffold. 
	@list=split(/\t/, $list);
	$pacid{$i}=$list[0];
	$query{$i}=$list[1];
	$start{$i}=$list[4];
	$stop{$i}=$list[5];
	$i++;
}

open (FILE, "./New_data_030613/Besthit.info") || die "nope\n";
	while ($line = <FILE>) { 
		@line=split(/\t/, $line);
		chomp $line[1];
		my @two = split(/\|/, $line[0]);
		$Laevisname{$line[1]} = join(':', $two[0], $two[1]); 
		print ("$Laevisname{$line[1]}\t");
	}
#    chomp;
#   push (@list, "$_"); 		#every line gets stored in an array
close (FILE);


close (IN);
while ($v < $i)  { 		#for each line in the array
 #   $pacid = $list[0]; 		#the first column is the pacid that the pseudogene is expected to align most closely to
  #  $query = $list[1]; 		#the second column is the Scaffold on which that pseudogene can be found
   # $start = $list[4]; 		#the location 500bp before where the pseudogene begins aligning to its paralog
   # $stop = $list[5];  		#the location 500bp after where the pseudogene ends aligning to its paralog
	
#	print ("$pacid $query $start");
	print ("$pacid{$v} $query{$v} $start{$v} $stop{$v} ");

	
	my $name = $pacid{$v};
	print ("$name\t $Laevisname{$name}\n");
  # ($query, $start, $stop) = split(/\t/, $v);
    
    push (@blastdbcmdLa, `blastdbcmd -db ~/Sahar_work/New_data_030613/XENLA_2012oct_prot_longest.fa -dbtype prot-entry "$Laevisname{$name}" -out "./proteinsL/$name.prot"`);

#	push (@blastdbcmdnt, `blastdbcmd -db ~/Laevis/Genome/LAEVIS_6.1.repeatMasked.fa -dbtype nucl -entry $query{$v} -range $start{$v}-$stop{$v} >> "./middle3/$name.practice.cds"`);
	#runs blastdcmd on Laevis genome finding location 500 bp +- where pseudogene is found
	
	push (@blastdbcmdprot, `blastdbcmd -db ~/Sahar_work/New_data_030613/Xtr_72_longest_pep.fa -dbtype prot -entry 'lcl|$pacid{$v}' >> "./proteinsT/$name.prot"`);
	#runs blastdcmd on Tropicalis proteome finding protein sequence for the ortholog of pseudogene

	push (@exonerate, `exonerate --exhaustive y --showalignment False --showvulgar False --ryo ">%ti (%tcb - %tce)\n%tcs\n" -S False -m protein2genome:bestfit ./middle2/$v.prot ./middle2/$v.nt > ./pseudoprot/$name`);
	# --ryo ">%ti (%tcb - %tce)\n%tcs\n"   
	
	# to get the dna sequence that aligns to protein w/o splice sites or introns or frameshifts. 
	
	#--ryo ">%ti (%tab - %tae)\n%tas\n"
	$v++;
}

foreach my $i (@blastdbcmdnt) {
    print "$i\n";
}
