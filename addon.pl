#!/usr/bin/env perl

use strict;
use warnings;
my @line;
my $line;
my $lines;
my @lines;


my @files;
my $file;
my $files;
my $name2;
my $name;
my %key;
my @transeq;

opendir (DH, "./cleanedpseudocds") or die "Cannot open: $! \n";

while ($file=readdir DH) {
	open (FILEH, "./cleanedpseudocds/$file") or die "nope1: $! \n"; 
	if ($file) {	
		push (@transeq, `transeq ./cleanedpseudocds/$file ./translatedpseudocds/$file`);
	} else {
#		print ("$file\t");
	}
}
close (FILEH);
closedir (DH);

open (IN, "Besthit.triplet") || die "nope3\n";
while ($lines = <IN>) {
	@lines=split(/\t/, $lines);
	chomp $lines[2];
	$name = ("$lines[1].cds.fasta");
	print ("$name\t$lines[2]\n");
	$key{$name}= $lines[2];
#	print OUT ("$lines[0]\t$lines[1]\t$lines[2]\n");
}

opendir (RH, "./translatedpseudocds") or die "Cannot open: $! \n";

while ($files=readdir RH) {
	open (TRANSLATED, "./translatedpseudocds/$files") or die "nope4: $! \n";
	print ("$files\t");
	if ($files =~ (/(\d{8})/) ) {
		$name2 = $1;
	}
	print ("$name2\t $files\t");
	open (ADDON, ">>./middleprot/$name2.prot");
	print ADDON (">$key{$files}\n");
	while ($line = <TRANSLATED>) { #read one line at a time
		$line =~ /\d+/ && next; #if it has a # go on to next line (common with gff files)
		print ADDON ($line);
	}
	close (ADDON);
	
}


close (TRANSLATED);
closedir (RH);