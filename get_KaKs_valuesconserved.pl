use warnings;
open (IN, "$ARGV[0]") || die "nope\n";
while (<IN>) {
    chomp;
    push (@list, "$_");
}
close (IN);
foreach $file (@list) {
    push (@R, `Rscript KaKs.R ./R/conserved/$file.fasta`);
    open (IN2, "./R/conserved/$file.fasta.values") || die "Cannot open .values file\n";
    while (<IN2>)
    {
	chomp;
	my $info = substr($_, 4, 8);
	push (@out, "$info");
    }
}
$count = 0;
foreach $value (@out)
{
    $count++;
    if ($count == 1)
    {
	`echo $value >> TC.KS`;
    }
    if ($count == 2)
    {
	`echo $value >> TP.KS`;
    }
    if ($count == 3)
    {
	`echo $value >> CP.KS`;
    }
    if ($count == 4)
    {
	`echo $value >> TC.KA`;
    }
    if ($count == 5)
    {
	`echo $value >> TP.KA`;
    }
    if ($count == 6)
    {
	`echo $value >> CP.KA`;
	$count = 0;
    }
}
