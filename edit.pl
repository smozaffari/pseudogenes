#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my @line;
my $newend;
my $newstart;
my @size;
my $size;
my %size;

open (EDITED, ">sortededited.psuedo") or die "Cannot open: $! \n";
open (SIZE, "L6.1.scaf_sizes") or die "$! \n";
open (PSEUDO, "sorted.psuedo") or die "$! \n";

while ($size = <SIZE>) {
	@size=split(/\t/, $size);
	$size{$size[0]}=$size[1];
}

while ($line = <PSEUDO>) { #read one line at a time
	@line=split(/\t/, $line); # split each word by tabs of each line into an element of an array
	$newstart= ($line[4] - 500);
	if ($newstart >1) {
		if ($newend < $size{$line[1]}) {
			print EDITED (join "\t", $line[0], $line[1], $line[2], $line[3], $newstart, $newend, $line[6], $line[7], $line[8] );
		}
	}
}

close(EDITED);
close(SIZE);
close(PSEUDO);
	