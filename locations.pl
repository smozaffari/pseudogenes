#!/usr/bin/env perl

use strict;
use warnings;

#my @list;
#my $list;
my $line;
my @line;
my %Laev;
my $Lprotein;
my %pseudoloc;
my %pseudostart;
my %pseudostop;
my %Tropstart;
my %Tropstop;
#my $tempID;
my %Trop;
my @two;
my $j;
my $i;
my $tropid;

open (THREE, ">locations.txt") || die "nope1\n";

open (IN, "./New_data_030613//Besthit.info") || die "nope2\n";
while (my $list = <IN>) { 		#reads in the filed with pseudogenes, sorted and added 500 bp on either side / on one scaffold. 
	my @list=split(/\t/, $list);
	$tropid = $list[1];	
	chomp $tropid;							 #tropapcid is key to hash for Laevis protein (longname)
	@two = split(/\|/, $list[0]);
	$Lprotein= $two[1]; #long Laevis protein

	$Laev{$Lprotein}= $list[0]; #key = laevis protein, answer = troppacid
	$Trop{$tropid}=$two[1]; #key= troppacid   value = laevisprotein
#	print ("$tropid\t $Trop{$tropid}\n ");
}
close (IN);

open (LAEVISA, "TropmRNA" ) || die "nope4\n"; #actually laevislocation
open (TWO, "Key") || die "nope3\n";
while (my $list=<TWO>) {
	my @list=split(/\t/, $list);
	$i = $list[0]; #key to all hashes is pacid.
	print ("$i\t $Trop{$list[0]}\n");
#	$pseudoloc{$i}=$list[1];
#	$pseudostart{$i}=$list[4];
##	$pseudostop{$i}=$list[5];
#	$Tropstart{$i}=$list[2];
#	$Tropstop{$i}=$list[3];
	
	while ($line = <LAEVISA>) {
		@line=split(/\t/, $line);
		($j) = $line[1]; #longlaevisprotein name
#		print ("$Trop{$list[0]}\t $j\n");
	#	$goodloc{$j}=$list[0];
	#	$goodstart{$j}= $list[2];
	#	$goodstart{$j}=$list[3]; #location of Laevis mrna of that protein
		#($tempID) = ($Laev{$j});
		if (($j) = ($Trop{$list[0]})) {
			print THREE ("$i\t $list[1]\t $list[2]\t $list[3]\t $list[4]\t $list[5]\t $Trop{$i}\t ");
			print ("$j \t $line[0]\n");
			print THREE ("$j\t $line[0]\t $line[2]\t $line[3]\n");
			last;
		}
	}
}
close (LAEVISA);
close (TWO);
close (THREE);

