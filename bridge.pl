#!/usr/bin/env perl

use strict;
use warnings;

my $size;
my @size;
my %size;
my @line;
my $line;
my %store;
my $pacID;
my %strand;
my @strand;
my $strand;
my %tstart;
my %tend;
my %start;
my %end;
my $t1info;
my $t2info;
my $l1info;
my $l2info;
my $ltotal;
my $ttotal;
my %orient;
my $first;
my $second;

open (MAP, "test28") or die "Cannot open: $! \n";
open (STRAND, "Nostrand.aligninfo" ) or die "Cannot open: $! \n";
open (SIZE, "L6.1.scaf_sizes") or die "$! \n";
open (BRIDGE, ">Bridge2.txt") or die "$! \n";
#open (OVERLAP, ">overlap.txt") or die "$! \n";

while ($size = <SIZE>) {
	@size=split(/\t/, $size);
	$size{$size[0]}=$size[1];
}

while ($strand = <STRAND>) {
	@strand=split(/\t/, $strand);
	$start{$strand[0]}=( $strand[1]);
	$end{$strand[0]} = ( $strand[2]);
	$tstart{$strand[0]}= ($strand[4]);
	$tend{$strand[0]}= ($strand[5]);
	$orient{$strand[0]}=($strand[7]);
}
	

while ($line = <MAP>) { #read one line at a time
	@line=split(/\t/, $line); # split each word by tabs of each line into an element of an array

	if ($orient{$line[1]}=~/-/) { #
		$l1info = $start{$line[1]};
		$t1info = $tend{$line[1]};
		print BRIDGE (" $line[1] -1 $l1info $t1info \t");
	} else { 
		if ($orient{$line[1]}=~/1/) {
			$l1info = (($size{$line[1]}) - ($end{$line[1]}));
			$t1info = $tend{$line[1]};
			print BRIDGE (" $line[1] +1 $l1info $t1info \t");
		} 
	}
	
	if ($orient{$line[3]}=~/-/) { #
		$l2info = (($size{$line[3]}) - ($end{$line[3]}));
		$t2info = $tstart{$line[3]};
		print BRIDGE (" $line[3] -1 $l2info $t2info \t");
	} else {
		if ($orient{$line[3]}=~/1/) { # 
			$l2info = $start{$line[3]};
			$t2info = $tstart{$line[3]};
			print BRIDGE (" $line[3] +1 $l2info $t2info \t");
		}
	}
	
	$ltotal= $l1info + $l2info;
	$ttotal= $t2info - $t1info;
	print BRIDGE (" L overhanging: $ltotal T bridge( $line[0]): $ttotal \t");
	my $diff = ($ttotal-$ltotal);
	print BRIDGE (" $diff \n");
#	} else {
#		if ($ttotal > $ltotal) {
#			my $diff = ($ttotal-$ltotal);
#			print BRIDGE (" T: $diff \n");
#		} else {
#			if ($ttotal = $ltotal) {
#				print BRIDGE (" Equal ");
#			}
#		}
	
#	}
}


close (MAP);
close (SIZE);
close (STRAND);
close (BRIDGE);
#close (OVERLAP);