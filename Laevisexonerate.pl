#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my @line;
my @id;
my %exonstart;
my %exonstop;
my %exon;
my @list;
my $list;
my %pacid;
my %query;
my %start;
my %stop;
my @blastdbcmdnt;   # blast data for blastdbcmd on genome for nucleotides of pseudogene
my @blastdbcmdnt2;
my @blastdbcmdprot2;
my @blastdbcmdprot; # blast data for blastdbcmd on proteome for protein sequence for paralog of pseudogene
my $i=1;
my $v=1;
my %Laevis;
my %notpseudo;
my @exonerate;
my @exonerate2;
my @exonerate3;
my @exonerate4;
my $newstart;
my $newend;
my %Laevisname;
my @two;
my $Laevis;
my $var;
my @temp;
my $pseudo;
my @pseudo;
my %pacids;
my $temp2;
my %start2; 
my %stop2;
my $newstart2;
my $newend2;
my %query2;
my $temp3;

open (IN, "norepeats.FLalign.info") || die "nope\n";
while ($list = <IN>) { 		 
	@list=split(/\t/, $list);
	
	$query{$i}=$list[3]; 					# scaffold that Laevis existing gene is on
	$start{$i}=$list[4]; 					# start of Laevis gene
#	chomp $list[5]; 						# removes newline character
	$stop{$i}=$list[5]; 					# stop of Laevis gene
	$Laevisname{$i}= $list[0]; 				# key = number that the gene is assigned ; hash = the pacid of the Laevisgene
	$pacid{$list[0]} = $list[1];			# key = Laevis ; hash = troppacid
	$Laevis{$list[1]} = $list[0];           # key = troppacid ; hash = Laevis
	$i++;
}

close (IN);

open (FILE2, "noIGC.pseudo.loc") || die "nope2 $!\n";
while ($pseudo = <FILE2>) { 
	@pseudo=split(/\t/, $pseudo); # get Laevis gene name
	print "$pseudo[0]\t";
	$temp3 = $pseudo[0];
	$pacids{$temp3} = 1;      # key = pacid #hash = 1 (true)
	$query2{$temp3} = $pseudo[1];
	$start2{$temp3} = $pseudo[2];
	chomp $pseudo[3];
	$stop2{$temp3} = $pseudo[3];
	
}
close (FILE2);
	
while ($v < $i)  { 	              # for each line in the array
	$var = $Laevisname{$v};       # $var = Laevisgene pacid
	if ($pacid{$var}) {           # if the troppacid for that Laevispacid exists,
		$temp2 = ($pacid{$var});  # assign trop pacid to a temp2 variable
		if ($pacids{$temp2}) {
		   # if it exists in the second hash, it means a corresponding pseudogene exists
		
			print ("$var $query{$v} $start{$v} $stop{$v} $pacid{$var}  \n");
		 	# prints all of the information for the existing Laevis gene

			$newstart = ($start{$v});
			$newend = ($stop{$v});
	
			push (@blastdbcmdnt, `blastdbcmd -db ~/Laevis/Genome/LAEVIS_7.1.repeatMasked.fa -dbtype nucl -entry $query{$v} -range $newstart-$newend -out "./Data/LaevisA/$pacid{$var}.nt"`);
			#runs blastdcmd on Laevis genome finding location 500 bp +- where existing Laevis gene is found
			
			push (@blastdbcmdprot, `blastdbcmd -db ~/Sahar_safe/Data/Xtr_72_longest_pep.fa -dbtype prot -entry 'lcl|$pacid{$var}' -out "./Data/LaevisA/$pacid{$var}.prot"`);
			#runs blastdcmd on Tropicalis proteome finding protein sequence for the paralog of pseudogene (same thing exists in folder Laevis2)
		
			push (@exonerate, `exonerate --showalignment False --showvulgar False -S False -m protein2genome --ryo ">%ti (%tcb - %tce)\n%tcs\n" ./Data/LaevisA/$pacid{$var}.prot ./Data/LaevisA/$pacid{$var}.nt > ./Data/cdsexonerate/$pacid{$var}`);
			# --ryo ">%ti (%tcb - %tce)\n%tcs\n"   
			
			push (@exonerate2, `exonerate --showalignment False --showvulgar True -S False -m protein2genome ./Data/LaevisA/$pacid{$var}.prot ./Data/LaevisA/$pacid{$var}.nt > ./Data/LaevisAexonerate2vulgar/$pacid{$var}`);
			# --ryo ">%ti (%tcb - %tce)\n%tcs\n"   
			
		
			$newstart2 = ($start2{$temp2});
			$newend2 = ($stop2{$temp2});
			
			print ("pseudo: $pacid{$var} $query2{$temp2} $newstart2 $newend2  \n");
		 	# prints all of the information for the existing Laevis gene

			push (@blastdbcmdnt2, `blastdbcmd -db ~/Laevis/Genome/LAEVIS_7.1.repeatMasked.fa -dbtype nucl -entry $query2{$temp2} -range $newstart2-$newend2 -out "./Data/LaevisB2/$temp2.nt"`);
			#runs blastdcmd on Laevis genome finding location 500 bp +- where existing Laevis gene is found
			
			push (@blastdbcmdprot2, `blastdbcmd -db ~/Sahar_safe/Data/Xtr_72_longest_pep.fa -dbtype prot -entry 'lcl|$pacid{$var}' -out "./Data/LaevisB2/$pacid{$var}.prot"`);
			#runs blastdcmd on Tropicalis proteome finding protein sequence for the paralog of pseudogene (same thing exists in folder Laevis2)
		
			push (@exonerate3, `exonerate --showalignment False --showvulgar False -S False -m protein2genome --ryo ">%ti (%tcb - %tce)\n%tcs\n" ./Data/LaevisB2/$pacid{$var}.prot ./Data/LaevisB2/$pacid{$var}.nt >> ./Data/cdsexonerate/$pacid{$var}`);
			# --ryo ">%ti (%tcb - %tce)\n%tcs\n"  
		
			push (@exonerate4, `exonerate --showalignment False --showvulgar True -S False -m protein2genome ./Data/LaevisB2/$pacid{$var}.prot ./Data/LaevisB2/$pacid{$var}.nt > ./Data/LaevisBexonerate2vulgar/$pacid{$var}`);
			# --ryo ">%ti (%tcb - %tce)\n%tcs\n" 

  
		}
			# cds = the dna sequence that aligns to protein w/o splice sites or introns or frameshifts. 
	}
		#CHANGED: to show vulgar output to parse for insertions/deletions
		$v++;
}

