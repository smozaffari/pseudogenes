#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my @line;
my @id;
my %exonstart;
my %exonstop;
my %exon;
my @list;
my $list;
my %pacid;
my %query;
my %start;
my %stop;
my @blastdbcmdnt;   # blast data for blastdbcmd on genome for nucleotides of pseudogene
my @blastdbcmdprot; # blast data for blastdbcmd on proteome for protein sequence for paralog of pseudogene
my $i=1;
my $v=1;
my @exonerate;
my $newstart;
my $newend;
my %Laevisname;
my @two;
my $Laevis;
my $var;
my @temp;
my $pseudo;
my @pseudo;
my %pacids;
my $temp2;

open (IN, "LaevisAgene") || die "nope\n";
while ($list = <IN>) { 		 
	@list=split(/\t/, $list);
	
	$query{$i}=$list[0]; 					# scaffold that Laevis existing gene is on
	$start{$i}=$list[2]; 					# start of Laevis gene
	chomp $list[3]; 						# removes newline character
	$stop{$i}=$list[3]; 					# stop of Laevis gene
	
	$Laevisname{$i}= $list[1]; 				# key = number that the gene is assigned ; hash = the name of the Laevisgene
	$i++;
}

close (IN);

open (FILE, "Besthit.info") || die "nope1 $! \n"; 
while ($line = <FILE>) { 
	@line=split(/\t/, $line); 						# get Laevis gene name
	chomp $line[1];
	my @two = split(/\|/, $line[0]);
 	$Laevis= (join "\|", $two[0], $two[1]); 		# removes JGIv6.000... after the second pipe
	$pacid{$Laevis}=($line[1]); 					# key = Laevisgene ; hash = pacid
	
	print ("$Laevis \t $pacid{$Laevis}\n"); 		# prints so we know it's working
													# Laevisgene name and pacid (key and hash)
}
close (FILE);

open (FILE2, "Tropinfo.candidates") || die "nope2 $!\n";
while ($pseudo = <FILE2>) { 
	@pseudo=split(/\t/, $pseudo); # get Laevis gene name
	print "$pseudo[0]\t";
	$pacids{$pseudo[0]} = 1;      # key = pacid #hash = 1 (true)
}
close (FILE2);
	
while ($v < $i)  { 	              # for each line in the array
	$var = $Laevisname{$v};       # $var = pacid
	if ($pacid{$var}) {           # if the Laevis name for that pacid exists,
		$temp2 = ($pacid{$var});  # assign it to a temp2 variable
		if ($pacids{$temp2}) {    # if it exists in the second hash, it means a corresponding pseudogene exists
		
			print ("$var $query{$v} $start{$v} $stop{$v} $pacid{$var} $pacid{$temp2} \n");
		 	# prints all of the information for the existing Laevis gene

			$newstart = ($start{$v} -500);
			$newend = ($stop{$v} +500);
	
			push (@blastdbcmdnt, `blastdbcmd -db ~/Laevis/Genome/LAEVIS_6.1.repeatMasked.fa -dbtype nucl -entry $query{$v} -range $newstart-$newend -out "./LaevisA/$pacid{$var}.nt"`);
			#runs blastdcmd on Laevis genome finding location 500 bp +- where existing Laevis gene is found
			
			push (@blastdbcmdprot, `blastdbcmd -db ~/Sahar_safe/Data/Xtr_72_longest_pep.fa -dbtype prot -entry 'lcl|$pacid{$var}' -out "./LaevisA/$pacid{$var}.prot"`);
			#runs blastdcmd on Tropicalis proteome finding protein sequence for the paralog of pseudogene (same thing exists in folder Laevis2)
		
			push (@exonerate, `exonerate --exhaustive y --showalignment True --showvulgar False -S False -m protein2genome:bestfit ./LaevisA/$pacid{$var}.prot ./LaevisA/$pacid{$var}.nt > ./LaevisAexonerate2/$pacid{$var}`);
			# --ryo ">%ti (%tcb - %tce)\n%tcs\n"   
		}
			# cds = the dna sequence that aligns to protein w/o splice sites or introns or frameshifts. 
	}
		#CHANGED: to show vulgar output to parse for insertions/deletions
		$v++;
}

