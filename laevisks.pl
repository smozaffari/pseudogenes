#!/usr/bin/env perl

use strict;
use warnings;
my $pacID;
my $line;
my @line;



open (KAKS, "KaKs") or die "$! \n";
open (NEW, ">LaevKs1") or die "$! \n";
while ($line = <KAKS>) { #read one line at a time
	$line =~ /#/ && next; #if it has a # go on to next line (common with gff files)
	@line=split(/\t/, $line); # split each word by tabs of each line into an element of an array
	print NEW ("$line[0] \t $line[1] \t ");
	if ($line[2] >= 1) { 
		print NEW ("1 \t");
	} else {
		print NEW ("$line[2] \t");
	}
	print NEW ("$line[3] \t $line[4] \t $line[5]\t $line[6] \t $line[7] \t $line[8] \t $line[9]");
}

close (KAKS);
close (NEW);