#!/usr/bin/env perl

use strict;
use warnings;

my @list;
my $pacid;
my $query;
my $start;
my $stop;
my @blastdbcmdnt; #blast data for blastdbcmd on genome for nucleotides of pseudogene
my @blastdbcmdprot; #blast data for blastdbcmd on proteome for protein sequence for paralog of pseudogene


open (IN, "sortededited.pseudo") || die "nope\n";
while (<IN>) { 		#reads in the filed with pseudogenes, sorted and added 500 bp on either side / on one scaffold. 
    chomp;
    push (@list, "$_"); 		#every line gets stored in an array
}
close (IN);
foreach my $v (@list) { 		#for each line in the array
    $pacid = $list[0]; 		#the first column is the pacid that the pseudogene is expected to align most closely to
    $query = $list[1]; 		#the second column is the Scaffold on which that pseudogene can be found
    $start = $list[4]; 		#the location 500bp before where the pseudogene begins aligning to its paralog
    $stop = $list[5];  		#the location 500bp after where the pseudogene ends aligning to its paralog

  # ($query, $start, $stop) = split(/\t/, $v);
  # push (@blastdbcmd, `blastdbcmd -dbtype nucl -db /house/homedirs/a/amsession/Xenopus/full_repeats.fa -entry $query`);

	push (@blastdbcmdnt, `blastdbcmd -db ~/Laevis/Genome/LAEVIS_6.1.repeatMasked.fa -dbtype nucl -entry $query -range $start - $stop -out $list[1]`);
	#runs blastdcmd on Laevis genome finding location 500 bp +- where pseudogene is found
	
	push (@blastdbcmdprot, `blastdbcmd -db ~/Sahar_work/Xenopus_laevisJGIL6RMv1.0.primaryTrs.longest_pep_PACID_name.fa -dbtype -prot -entry 'lcl|$pacid' -out $pacid`);
	#runs blastdcmd on Laevis proteome finding protein sequence for the paralog of pseudogene
}
foreach my $i (@blastdbcmdnt) {
    print "$i\n";
}
