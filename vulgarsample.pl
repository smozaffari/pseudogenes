#!/usr/bin/env perl

#use strict;
use warnings;

#pacid: insertion: length; exon # / total exons

my @line;
my $line;
my @id;
my %exonstart;
my %exonstop;
my %exon;
my $intron=0;
my @intron;
my $length;
my $exon;
my $size;
my $insertion;
my $deletion;
my $id;
my $info;
my @name2;
my @gene;
my $intronnum;
my $var=0;
=head

open (GFF, "./output/gff20884595 ") or die "Cannot open: $! \n";
open (FILE, ">SAMPLEGFF") or die "$! \n";
while ($line = <GFF>) { #read one line at a time
	$line =~ /#/ && next; #if it has a # go on to next line (common with gff files)
	if ($line =~ /Command/) {
		$var=1;
	}
	$line =~ /Hostname/ && next;
	$line =~ /-- / && next;
	@line=split(/\t/, $line); # split each word by tabs of each line into an element of an array

	if (($line[2])=~ /gene/) { #if the piece is a gene/ says gene
#		$info = ($line[8] =~ m/\d{8}/);

		if ($line[8] =~ /\;/) {
			my $name = $line[8];
			print $line[8];

			@name =split(/\;/, $line[8]);
			@name2 =split(/\|/, $name[1]);
			$info = ($name2[1]);

	}
	if (($line[2])=~ /exon/) {
		print $info;
		($exonstart{$info}{$var}) = $line[3]; #record start by pacID number
		($exonstop{$info}{$var})= $line[4]; #record stop by pacID number
		($exon{$info}{$var})=($line[4]-$line[3]+1);
		print FILE ("$info \t $var\t $exon{$info}{$var}\n");
		$var++;
	} 

}
close(FILE);
close(GFF);

=cut
open (IN, "./LaevisBvulgar") || die "nope\n";
open (DEL, ">DELETIONS") or die "$! \n";
open (INS, ">INSERTIONS") or die "$! \n";
while ($line = <IN>) {
	@gene=split(/\n/, $line);
	#print ("$gene[0] \n $gene[1] \n $gene[2] \n");
	$count = @gene;
#	print $count;
	foreach (my $j=0; $j <= $count; $j++ ){
	#	print ("$gene[$j] \n");
		
	#if (defined($element) && ($element =~ /$pattern/))
		if (defined($gene[$j]) && ($gene[$j]=~ m/vulgar: /)) {
			@intron=split(/I /, $gene[$j]);
			#print $intron;
			$intronnum = @intron;
			$exon = $intronnum+1;
			if ($intron[0] =~ m/lcl\|(\d+)/) {
				$id = $1;
			}
			foreach (my $i =0; $i < $intronnum; $i++) {
				my $ex = $i+1;
				while ($intron[$i] =~ m/ F (\d+) (\d+) /gi) {	
					$length = $2;
					print INS ("$id  insertion  $length  $ex $intronnum \n");
					$insertion++;
				}
				while ($intron[$i] =~ m/ G (\d+) (\d+)/gi) {
					if ($1 ==0) {
						$length = $2;
						print INS ("$id  insertion  $length  $ex  $intronnum \n");
					} else {
						$length = $1*3;
						print DEL ("$id  deletion  $length  $ex  $intronnum \n");
						$deletion++;
					}
					
				}
			}
	
		} 
	}
}

close (IN);