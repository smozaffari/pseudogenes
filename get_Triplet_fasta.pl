use warnings;
open (IN, "$ARGV[0]") || die "nope\n";
while (<IN>) {
    chomp;
    push (@list, "$_");
}
close (IN);
foreach $file (@list) {
    push (@blastdbcmd, `blastdbcmd -dbtype nucl -db ./Data/Xtr72.CDS.fa -entry 'lcl|$file' | sed 's/lcl|//' > ./R/"$file".fasta`);
    push (@LA, `perl get_Exonerate_CDS.pl ./Data/AdamLAe/$file conserved >> ./R/"$file".fasta`);
    push (@LB, `perl get_Exonerate_CDS.pl ./Data/LaevisBexonerate/$file pseudo >> ./R/"$file".fasta`);
}
