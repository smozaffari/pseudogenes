#!/usr/bin/env perl

#use strict;
use warnings;

#pacid: insertion: length; exon # / total exons

my @line;
my $line;
my $intron=0;
my $length;
my $exon;
my $size;
my $insertion;
my $deletion;
my $id;

open (IN, "vulgar1") || die "nope\n";
$line = <IN>;	

#print $intron;
@line=split(/I /, $line);
$intron = @line;
print $intron;
$exon = $intron+1;

print ("$line[4]\n ");

if ($line[0] =~ m/lcl\|(\d+)/) {
	$id = $1;
}

foreach (my $i =0; $i < $intron; $i++) {
	my $ex = $i+1;
	while ($line[$i] =~ m/ F (\d+) (\d+) /g) {
		$length = $2;
		
		print ("$id: insertion: $length: $ex: $intron \n");
		$insertion++;
	}
	
	while ($line[$i] =~ m/ G (\d+) (\d+) /g) {
		$length = $1*4;
		print ("$id: deletion: $length: $ex: $intron \n");
		$deletion++;
	}
}

#for (my $i =0; $i < @line; $i++) {

#	if ($line =~ m/F (\d+) (\d+)/) {
#		$intron++;
#		$length = $2;
#		print $length;
#	}
#}

#	@line=split(/\s+/, $line);
#	if ($line =~ m/I\ \d*\ \d*/)  { 
#		$intron++;
#	} 
#	print "$line[0] $line[9]\t";
#	print "$exon $intron \n";
close (IN);