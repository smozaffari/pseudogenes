#!/usr/bin/env perl

=head

4/1/13: rerun pseudoTrLbcds on ssh. will rename all cds from pseudo genes in middleprot folder
then will use file names to get to corresponding file names in different folders.
due 4/3/13
=cut

use strict;
use warnings;
my @line;
my $line;
my $lines;
my @lines;
my $lined;
my @lined;
my @files;
my $file;
# my $name;
my %key;
my @transeq;


opendir(DH, "./pseudoprot") or die "Cannot open: $! \n";

while ($file=readdir DH) {
	open (FILEH, "./pseudoprot/$file") or die "nope1: $! \n"; 
	
	open (OUT, ">./cleanedpseudocds/$file") or die "nope2: $! \n";


while ($line = <FILEH>) { #read one line at a time
		$line =~ /Command/ && next; #if it has a # go on to next line (common with gff files)
		$line =~ /%tcs/ && next;
		$line =~ / False/ && next;
		$line =~ /Hostname/ && next;
		$line =~ /completed/ &&next;
		print OUT ($line);
	}
	close (FILEH);
	close (OUT);
}
closedir (DH);

open (IN, "Besthit.triplet") || die "nope3\n";
while ($lines = <IN>) {
	@lines=split(/\t/, $line);
	chomp $lines[2];
	$key{$lines[1]}= $lines[2];
#	print OUT ("$lines[0]\t$lines[1]\t$lines[2]\n");
}

close (IN);


push (@transeq, `transeq ./cleanedpseudocds/$file.cds.fasta ./translatedpseudocds/$file`);
	
	
opendir (RH, "./translatedpseudocds") or die "Cannot open: $! \n";

while ($file=readdir RH) {
	open (TRANSLATED, "./translatedpseudocds/$file") or die "nope4: $! \n";
	open (ADDON, "./proteinsLT/$file.prot");
	print ADDON (" $key{$file}");
	while ($lined = <TRANSLATED>) { #read one line at a time
		$lined =~ /\d+/ && next; #if it has a # go on to next line (common with gff files)
	}
	print ADDON ($lined);
}

close (ADDON);
close (TRANSLATED);
closedir (RH);