#!/usr/bin/perl -w

%Movies = ( 'The Shining', 'Kubrick', 'Ten Commandments', 'DeMille', 'Goonies', 'Spielberg');

%ByDirector = reverse %Movies;

print (join ' ',%ByDirector);
print "\n";

@Directors=values %ByDirector;
@Films=keys %ByDirector;
 
print (join ' ', @Directors);
print "\n";
print (join ' ', @Films);
print "\n";

