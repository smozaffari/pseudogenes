#!/usr/bin/env perl

use strict;
use warnings;

open (MYFILE, "testfile") || die "opening testfile: $!";
my @lines=<MYFILE>;
close(MYFILE);
my @words = map { split ' ' } @lines;
foreach (@words) {
    if (m/[^aeiouAEIOU]{4}/) {
		print "$_ has four consonants in a row. \n";
	}
}
