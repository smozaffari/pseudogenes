#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my @line;
my @id;
my $counter;
my $counter2;
my $list;
my @list;
my %pseudo;
my $pacid;
my $line2;
my @line2;


open (IN, "Tropinfo.candidates") || die "nope\n";
while ($list = <IN>) { 		#reads in the filed with pseudogenes, sorted and added 500 bp on either side / on one scaffold. 
	@list=split(/\t/, $list);
	$pacid=$list[0];
	$pseudo{$pacid}=1;
	open (OUT, ">./aligned/$pacid");
	print OUT ( "$pacid");

	open (FILE2, " ./cdsproteins/$pacid.cds") or die "cannot open: $!\n";
	$counter2=0;
	while ($line2 = <FILE2>) {
		if ($line2 =~ />/){
			print OUT ("\t");
			$counter2 ++;
			next;
		 } else {
			chomp $line2;
		 	print OUT ($line2);
		 	$counter2++;
		 }
	}
	close (OUT);
	close (FILE2);
}
close (IN);