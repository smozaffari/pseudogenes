#!/usr/bin/perl -w

open (PH, "customers.txt") or die "Cannot open customers.txt: $!\n";
while (<PH>) {
	chomp;
	($name, $number, $email) = ( split(/\s+/, $_) )[0,1,2];
	$Name{$name}= $_;
	$Phone{$number} = $_;
	$Email{$email} = $_;
}
print "Type 'q' to exit\n";
while (1) {
	print "\nName?";
	$name= <STDIN>; chomp ($name);
	$number = 0;
	$address = "";
	if (! $name) {
	
		print "\nNumber? ";
		$number = <STDIN>; chomp ($number);
	
			if (! $number) {
				print "\nE-Mail? ";
				$address = <STDIN>; chomp($address);
			}
	}
	last if ($name eq 'q' or $number eq 'q' or $address eq 'q');	
	
	if ($name and exists $Name{$name}) {
		print "Customer: $Name{$name}\n";
	}
	
	if ($name) {
		foreach ( keys %Name ) {
			if (/$name/i) {
				print "Customer: $Name{$_}\n";
			}
		}
	}
	if ( $number and exists $Phone{$number} ) {
		print "Customer: $Phone{$number}\n";
		next;
	}
	if ($number) {
		foreach (keys %Phone) {
			if (/$number/) {
				print "Customer: $Phone{$_}\n";
			}
		}
	}
	if ($address and exists $Email{$address} ) {
			print "Customer: $Email{$address}\n";
			next;
	}
	if ($address) {
		foreach (keys %Email) {
			if (/$address/) {
				print "Customer: $Email{$_}\n";
			}
		}
	}
	if (! $name and ! $number and ! $address) {
	print "Customer record not found. \n";
	next;
	}
}
print "\nAll done.\n";
