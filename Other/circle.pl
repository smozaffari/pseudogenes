#!usr/bin/perl -w

print "what is the radius of the circle? ";
$radius=<STDIN>;
if ($radius <= 0)	{
	print "The circumference is 0.\n";
}	else	{
	$circumference=2*(3.141592654)*$radius;
	print "The circumference is $circumference.\n";
}