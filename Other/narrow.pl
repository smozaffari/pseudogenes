#!/usr/bin/env perl

use strict;
use warnings;
my %scaffold;
my $lscaff;
my $tscaff;
my $Laevis;
my $num;
my %tscaffold;
my %rank;
my $big;
my %gene;
my %highest;
my $bothabove10=0;
my $onebelow10=0;
my %count;

open (TEN, ">ten") or die "Cannot open: $!";
open (NEG1, ">neg1") or die "Cannot open: $!";
open (POS1, ">pos1") or die "Cannot open: $!";
open (BELOW10, ">below10") or die "Cannot open: $!";
open (NARROW, ">narrow") or die "Cannot open: $!" ;
open (MORE1MATCH, "testout") or die "Cannot open: $!" ;

while (my $line = <MORE1MATCH>) {
	my @line=split(/\t/, $line);
	$lscaff=$line[0];
	$tscaff=$line[1];
	if ($scaffold{$lscaff}) {
		my $i=1;
		my $counting=0;
		do {
			$i++;
		} until ( ! $scaffold{$lscaff}{$i} );
		($scaffold{$lscaff}{$i}) = ($line[2]);
		($tscaffold{$lscaff}{$i}) = ($line[1]);
		my $minus=($i-1);	
		if (($scaffold{$lscaff}{$i}) > ($gene{$lscaff}) ) {
			($gene{$lscaff})=($i);
			($highest{$lscaff})= ($tscaffold{$lscaff}{$i}) ;
		}
		if (($tscaffold{$lscaff}{1}<11 )){
			print TEN (join " ", @line);
		}
	} else {
		($scaffold{$lscaff}{1}) = ($line[2]);
		($tscaffold{$lscaff}{1}) = ($line[1]);
		($highest{$lscaff})= ($line[1]);
		($gene{$lscaff})=($line[2]);
		if (($tscaffold{$lscaff}{1}<11 )){
			print TEN (join " ", @line);
		}
	}
}

foreach $Laevis ( sort keys %scaffold) { #sort keys and for each key print pacid number and each collapsed info of scaffold with tab between
#	print NARROW ("$Laevis \t $highest{$Laevis}");
	my $i=1;
	my $counting=1;
	do {
		if ($tscaffold{$Laevis}{$i} <11) {
			$counting++;			
		}
		$i++;
	} until ( ! $scaffold{$Laevis}{$i} );
	if ($counting == $i) {
		$count{$Laevis} = (-1);
		print BELOW10 ("$Laevis -1 \n");
	} else {
		$counting=$counting-1;
		$i=$i-1;
		$count{$Laevis} = ($counting);
		print BELOW10 ("$Laevis $counting $i \n");
	}
	my $c=1;
	while ($scaffold{$Laevis}{$c}) {
		for $num ( keys %{ $scaffold{$Laevis} } ) {
			if ( $c!=$num) {
				my $value= (($scaffold{$Laevis}{$c}) - ($scaffold{$Laevis}{$num}));
				if ($value >= 0) {
					print NARROW ("$Laevis \t $highest{$Laevis} \t $value \n");
					if ($count{$Laevis}== -1) {
						print NEG1 ("$Laevis \t $highest{$Laevis} \t $value \n");
					}
					if ($count{$Laevis}==1) {
						print POS1 ("$Laevis \t $highest{$Laevis} \t $value \n");
					}
#					print NARROW (" \t $value");					
				}
			}
		}
		$c++;
	}	
#	print NARROW ("\n");
}


=head
foreach $Laevis ( sort keys %scaffold) {
	my $i=1;
	my $counting=1;
	do {
		if ($tscaffold{$Laevis}{$i} <11) {
			$counting++;			
		}
		$i++;
	} until ( ! $scaffold{$Laevis}{$i} );
	if ($counting == $i) {
		print BELOW10 ("$Laevis -1 \n");
	} else {
		$counting=$counting-1;
		$i=$i-1;
		print BELOW10 ("$Laevis $counting $i \n");
	}
}
		
	my $e=1;
	my $lastL=0;
	my $lasts=0;
	foreach (keys %{scaffold{$Laevis} } ) {
		if ($_>10) {
			if $lasts {
				if ($lasts <10) and ($scaffold{$Laevis}{$lasts} {
					$oneabove10=$oneabove10+1;
				}
			}
		
		} else {
			if ($_<10) {
				if $lasts {
					if ($lasts >10) and ($scaffold{$Laevis}{$lasts}) {
						$oneabove10=$oneabove10+1;
					}
				}
			}
		}
	}$lasts=$_;
	$lastL=$Laevis;
}
=cut
		
		
		
		
close (BELOW10);
close (NARROW);
close (MORE1MATCH);
close (TEN);
close (NEG1);
close (POS1);