#!/usr/bin/env perl

use strict;
use warnings;
my $line;
my @line;
my %scaffold;
my $count;
my $lscaffold;
my $tscaffold;
my $genematch;
my @info;
my $lscaff;
my $countgenes;
my @tropscaf;
my @stropscaf;
my %tscafhash;
my %lscafhash;
my %genemat;
my $timer;
my $key1;
my $key2;
my $final;
my %count;
my $difference;
my $geneone;
my $genetwo;
my $tdigits;
my $ldigits;

=head

this is free text, type whatever you want


=cut
open (NEW, ">newone") or die "Cannot open: $!";
open (TWOMATCHES, ">twomatches" ) or die "Cannot open: $!";	#file to print the ones that match 2 trop scaffolds
open (BESTMATCH, ">bestmatch" ) or die "Cannot open: $!"; #file to print the best matches-all
open (GENES, ">genes") or die "Cannot open: $!";
open (DUPLICATEGENES, ">duplicategenes") or die "Cannot open: $!";
open (ZERO, ">zeroabove") or die "Cannot open: $!";
open (COLLAPSE, "sortedcollapsed") or die "Cannot open: $!";
while ($line = <COLLAPSE>) { #read in one line at a time
	@line=split(/\t/, $line); #split each number and assign to array.
	($lscaffold)=$line[0]=~/(\d+)/; #isolate L scaffold number
	($tscaffold)=$line[1]=~/(\d+)/; #isolate T scaffold number that L scaffold matches to
	($genematch)=$line[2]=~/(\d+)/; #isolate number of genes that match L scaffold to T scaffold
#	($ldigits)=$line[0]=~/(\d+)/;
#	($tdigits)=$line[1]=~/(\d+)/;
	if ( exists $lscafhash{$lscaffold}  ) { #if scaffold matches to more than one trop scaffold.
		$count{$lscaffold} = ($count{$lscaffold} +1) ; #of matches to diff tropscaffolds
		$genemat{$lscaffold}{$tscaffold} = $genematch; #assign number of genes that match tscaff to lscaff
		$lscafhash{$lscaffold}{$tscaffold} = (join "\t", ($line[3], $line[4], $line[5], $line[6]));
		push @ { $tscafhash {$lscaffold} }, $tscaffold; #adds on the new trop scaffolds that match
		if ($tscaffold <11) { 							# if matches to tropscaffold 1-10 records it
			$scaffold{$lscaffold}{'under'}=($scaffold{$lscaffold}{'under'})+1;
		} else {
			$scaffold{$lscaffold}{'above'}=($scaffold{$lscaffold}{'above'})+1;
		}
		$genemat{$lscaffold}{B}=$genematch; #assign second and more matches (replace) to key2 - only used if 2.
		$lscafhash{$lscaffold}{B}=$tscaffold; #keep second trop scaffold somewhere
	} else {
		$count = 1;
		$count{$lscaffold} = 1;
		$genemat{$lscaffold}{A}=$genematch; #assign first match to tscaffold as key1
	#	$genemat{$lscaffold}{B}=0;
		$lscafhash{$lscaffold}{A}=$tscaffold; #keep first trop scaffold number somehwere
		@tropscaf=$tscaffold;
		if ($tscaffold <11) {
			$scaffold{$lscaffold}{'under'} = 1;
			$scaffold{$lscaffold}{'above'} = 0;
		} else {
			$scaffold{$lscaffold}{'above'} = 1;
			$scaffold{$lscaffold}{'under'} = 0;
		}
		$tscafhash{$lscaffold} = [ @tropscaf ];
		$genemat{$lscaffold}{$tscaffold} = $genematch; #assign number of genes hash within hash to 1
		$lscafhash{$lscaffold}{$tscaffold} = (join "\t", ($line[3], $line[4], $line[5], $line[6])); #keep all other info with hash
	}
}	

my @sortedkeys= sort keys %scaffold; #sort based on scaffold number, same scaffolds next to each other
foreach $lscaff (@sortedkeys) { #go through each Lscaffold
	$final = ( $lscafhash{$lscaff}{A} ) ;
	print NEW (join "\t", ($lscaff, $count{$lscaff}));
	print NEW ("\n"); 
	if ( exists ($genemat{$lscaff}{B}) ) { #if there are only 2 matches
		$geneone = ($genemat{$lscaff}{A});
		$genetwo = ($genemat{$lscaff}{B});
		$difference = ( $geneone - $genetwo); #record difference
		#print TWOMATCHES ($lscaff, "\t", abs($scaffold{$lscaff}{'diff'}), "\n"); #record the lscaffold # and difference 
		#if ( abs($difference) > 5 ) { #if the difference between gene match > 5
			if (($genetwo) > ($geneone)) { #1st one is bigger, if not, 2nd one is true match.
				$final = ($lscafhash{$lscaff}{B});
			} else {
				$final = ($lscafhash{$lscaff}{A});
			}	
		#}	
				# lscaff # 		# of diff trop/laev matches			# of matches to trop1-10		# of matches to trop>11
		print DUPLICATEGENES ("Scaffold", $lscaff, "\t", $count{$lscaff},"\t", $scaffold{$lscaff}{'under'}, "\t", $scaffold{$lscaff}{'above'}, "\t");
		print DUPLICATEGENES (join " ", (@{$tscafhash{$lscaff}})); #list of all the trop scaffolds
		print DUPLICATEGENES ("\n");
		if ($scaffold{$lscaff}{'above'} == 0) {
			print ZERO ("Scaffold", $lscaff, "\t", $count{$lscaff}, "\t"); #lscaff #, # of diff matches to trop
			foreach my $tttt (@{$tscafhash{$lscaff}} ){
				my $name= ("scaffold_$tttt");
				print ZERO ( join ":  ", ($name, $lscafhash{$lscaff}{$tttt}));
				print ZERO ("\t");
			}
			print ZERO ("\n");
		#	print ZERO ( join "\n ", ( @{ $tscafhash{$lscaff}} )); #each trop scaffold it matches to
		#	print ZERO ( $final ); #all the information: 
		#	print ZERO ("\n");      #lscaffold#, # of diff trop scaffolds, #tscaffold, tstart, tstop, lstart and lstop.
		}
	}
	print GENES ("Scaffold", $lscaff, "\t", $count{$lscaff}, "\n");
	print BESTMATCH ("Scaffold", $lscaff, "\t", $final, "\t", $count{$lscaff}, "\t", $lscafhash{$lscaff}{$final}, "\n"); #prints best genes match: laevis scaffold # and trop scaffold n# and # of matching genes	
}				#^laevis scaf #    #tscaff#			# of genes #scaffold number and start and stop of trop and start and stop on Laevis. 	
close (TWOMATCHES); #close files.
close (COLLAPSE);
close (BESTMATCH);
close (GENES);
close (DUPLICATEGENES);
close (ZERO);
close (NEW);
		
	 