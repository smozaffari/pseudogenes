#!/usr/bin/env perl

use strict;
use warnings;
my $line;
my @line;
my %lscaffold;
my $scaffold;
my $num;
my $count=1;
my $length=0;

open (LINKED, "final") or die "Cannot open: $!";
while ($line = <LSCAFFOLD>) {
	@line=split(/\t/, $line);	
		$num = join ":"($line[4]:$line[1]);
		if ( exists $scaffold{$num} ) {
			$count++ ;
			$scaffold{$num}{'genes'} = ($count);
			if ($line[2]<($scaffold{$num}{'tstart'} ) {
				$scaffold{$num}{'tstart'} = $line[2];
			}
			if ($line[3]>($scaffold{$num}{'tstop'} ) {
				$scaffold{$num}{'tstart'} = $line[3];
			}
			if ($line[5]<($scaffold{$num}{'lstart'} ) {
				$scaffold{$num}{'lstart'} = $line[5];
			}
			if ($line[5]>($scaffold{$num}{'lstop'} ) {
				$scaffold{$num}{'lstop'} = $line[6];
			}
		} else
			$count=1;
			$scaffold{$num}{'genes'} = ($count);
			$scaffold{$num}{'tstart'} = ($line[2]);
			$scaffold{$num}{'tstop'} = ($line[3]);
			$scaffold{$num}{'lstart'} = ($line[5], 
			$scaffold{$num}{'lstop'} = ($line[6]);
			$scaffold{$num}{$line[0]} = ($line[0]);
			
		}	
}