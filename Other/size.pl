#!/usr/bin/env perl

use strict;
use warnings;
my $numargs = @ARGV; # xtr_sorted, L6.1, sortbygene
if ($numargs != 3) { 						# makes sure 3 files were used as arguments
    print "\not enough files\n";			# first file has Laev pacids matched to trop scaffolds
    exit;									# second file has size of all Laevis scaffolds
}			# third file has list of Laevis scaffolds & trop scaffold match, with # of genes, location and pacid#'s
			# ^ needs to be sorted from largest # of genes to lowest.

my $counter = 0;
open (SUM, $ARGV[0]) or die "Cannot open: $!";  # Pacid#(each Lgene) Tscaffold #  
while (my $line = <SUM>) {  # read through all *matched* Laev scaffolds and pacids. 
	$counter++;				# counts the total number of laevis genes that also match to trop.
}

my $xcount=0;
close (SUM);

print ("\n", $counter, "\t The total number-of-laevis-genes-with-trop-orthologs \n");
$counter=($counter)/2;		# divide by 1/2, need to determine how many genes to be halfway
print ($counter, "\t Half of the total number-of-laevis-genes-with-trop-orthologs \n");

my $countgenes =0 ;
my $xvalue;
my $scaff =0;
my $bigscaff=0;

my %scaffold;
open (LSIZE, $ARGV[1]) or die "Cannot open: $!";	# LScaffold# size
	while (my $size= <LSIZE>) { 				# read through file with Laevis scaffolds and size
		my @size=split(/\t/, $size);			
		my $Lsize= $size[0];					
		$scaffold{$Lsize}=($size[1]);			# save size as value to hash with scaffold as key
#		print ($Lsize, "\t", $scaffold{$Lsize});# list scaffold and size to make sure it worked
	}
close (LSIZE);

open (COLLAPSE, $ARGV[2]) or die "Cannot open: $!";
my %match;
my $matchsize =0;
my %countscaf;

while (my $read = <COLLAPSE>) {			
	my @read=split(/\t/, $read);	 	# read through file that has matched Laev scaffolds and gene numbers		
	my $Lscaf=$read[0];				
	if (exists $scaffold{$Lscaf} ) { 	# foreach Laevis scaffold that is read in
		if ( exists $match{$Lscaf} ) {  # make sure to read only once, if already read and assigned skip
		} else {
			$xcount++;		# count number of Lscaffolds that match trop scaffolds, no repeats 
							# if Lscaffolds matches to more than one trop location, only records one
			$match{$Lscaf} = ($scaffold{$Lscaf}); # key = Lscaffold#, value = size - only for matched
	#		print ($Lscaf, "\t", $scaffold{$Lscaf}); 
			$matchsize=($matchsize+($scaffold{$Lscaf})); #size sum of all scaffolds that match
		}
	}
}
close (COLLAPSE);
open (SIZE, ">countdown") or die "Cannot open: $!";
open (COLLAPSE, $ARGV[2]) or die "Cannot open: $!"; # LScaffold# Tscaffold# #ofgenes
until ($countgenes >= $counter ) {		  	 # keep reading file until reach halfway through total genes
	while (my $read = <COLLAPSE>) {		
		my @read=split(/\t/, $read);
		$countgenes=($countgenes+ $read[2]); # add up the sizes of genes to determine when you reach 1/2
	    if ($countgenes <= $counter) {		 # keep going unless past halfway
	    	$scaff++; 						 # Total number of scaffolds to reach N50 number			
	    	$xvalue = $read[2];				 # N50 number, reassigned until reached halfway
	    	my $scafold = $read[0];			 # isolated Lscaffold number
	    	$bigscaff = ($bigscaff + $scaffold{$scafold}); # use Lscaffold number to add up scaffold sizes
	    	print SIZE ($xvalue, " ", $bigscaff, "\t");
	    }												   # 
		if ($countgenes >= $counter) {
		 	next;
		 }
	}
}
close (SIZE);
close (COLLAPSE);

print ($xcount, "\t The total number of scaffolds that have at least one laevis-genes-with-trop-ortholog \n");
print ($matchsize/(1000000000), "\t The total length of these scaffolds in Gbp \n");
print ($xvalue, "\t N50 number for genes/typical genes per scaffold \n");
print ($scaff, "\t Total number of Laevis scaffolds to reach N50 number \n");
print ($bigscaff/(1000000000), "\t The total length of Laevis scaffolds to reach N50 number \n");

