#!/usr/bin/perl -w

print "Monthly deposit amount? ";
$pmt=<STDIN>;
chomp $pmt;

print "Annual Interest rate? (ex. 7% is 0.07) ";
$interest=<STDIN>;
chomp $interest;

print "Number of months to deposit? ";
$mons=<STDIN>;
chomp $mons;

#Formula requires a monthly interest
$interest/=12;

$total=$pmt * ( ( ( 1 + $interest) ** $mons ) -1 )/$interest;

#Take integer of total
$totalint=int($total); 

#Turn two digits of decimal into integer then back to decimal
$remainder=$total-$totalint; 
$remainder=$remainder*100; 
$newremainder=int($remainder); 
$newremainder/=100;

#Total amount to two decimal places
$newtotal=$totalint + $newremainder;

print "After $mons months, at $interest monthly you\n";
print "will have $newtotal.\n";