#!/usr/bin/env perl

use strict;
use warnings;
my $pacID;
my $line;
my @line;



open (KAKS "KaKs") or die "$! \n";
open (NEW ">NewKaKs") or die "$! \n";
while ($line = <KAKS>) { #read one line at a time
	$line =~ /#/ && next; #if it has a # go on to next line (common with gff files)
	@line=split(/\t/, $line); # split each word by tabs of each line into an element of an array
	print NEW ("$line[0] \t $line[1] \t $line[2] \t");
	if ($line[3] >= 1) { 
		print ("1 \t");
	} else {
		print ("$line[3] \t");
	}
	print NEW ("$line[4] \t $line[5]\t $line[6] \t $line[7] \t $line[8] \t $line[9] \n");
}
