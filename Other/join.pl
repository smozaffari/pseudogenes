#!/usr/bin/perl -w

%first=('a', '1', 'b', '2');
%second=('c', '3', 'd', '4');
%Both= (%first, %second);

print keys %Both;
print "\n";
print values %Both;
print "\n";

%Additional = (%Both, e => '5', f => '6');

%Additional=sort %Additional;

print sort keys %Additional;
print "\n";
print sort values %Additional;
print "\n";