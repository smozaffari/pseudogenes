#!/usr/bin/perl -w

open (MYFILE, "testfile") || die "opening testfile: $!";
@lines=<MYFILE>;
close(MYFILE);
@words= map { split } @lines;
foreach $word (@words) {
	if ($word =~/[^aeiouAEIOU]{4}/) {
		print "$word has four consonants in a row. \n";
	}
}