#!/usr/bin/env perl

use strict;
use warnings;

my %line;
my $line;
my $A =0; 
my @invalid = ();
my %info;

open (FILE, "12313") or die "Cannot open: $!" ;
open (RESULT, ">result") or die "$! \n";

while (my $line = <FILE>) {
	chomp $line;
	my @line=split(/\t/, $line);
	$A++;
	$info{$A}=[ $line[0], $line[4], $line[5] ];
}

my $total = $A+1;
my $start = $info{1}[1];
my $end = $info{1}[2];

for (my $l=2; $l < 100; $l++) {
#	print (" $info{$l}[1] $info{$l}[2] \n");
	if (($info{$l}[1] > $start ) and ($info{$l}[1] < $end)) {
		if (($info{$l}[2]) > $end) {
			my $m= $l-1;
			print RESULT (" $info{$m}[0] $info{$l}[0]	$start  $info{$l}[2] \n");
		}  
	
	}
	$end = ($info{$l}[2]);
	$start = ($info{$l}[1]);
}


close (FILE);
close (RESULT);