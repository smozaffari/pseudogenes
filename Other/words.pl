#!/usr/bin/env perl

use strict;
use warnings;

open (WORDS, "words.txt") or die "Cannot open $!\n";
my @lines = <WORDS>;
close (WORDS);
my @words = map { split ' ' } @lines;
map {s/[.,]//g } @words;
print @words;

foreach @words {
	my $number=0;
	for ($j=1; $j++) {
		if (m/w{$j}/) {
			$word{$j}=$number+1;
		}
	}
}

foreach @words {
	