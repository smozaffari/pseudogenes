#!/usr/bin/env perl

use strict;
use warnings;
my $line;
my @line;
my %lscaffold;
my %scaffold;
my $num;
my $count=1;
my $length=0;
my $Laevis;
my @info;
my @pacid;
my $a;
my $ldigits;
my $tdigits;
my @distance;
my $countg;
my $apart;
my @pacids;

open (LINKED, "final") or die "Cannot open: $!";
while ($line = <LINKED>) {
	@line=split(/\t/, $line);	
		($ldigits)=$line[4]=~/(Scaffold\d+)/;
		($tdigits)=$line[1]=~/(\d+)/;
		$apart = $line[3];
		if ($line[6]-$line[5] > 2*($line[3]-$line[2])) {
			next;
		}
		$num = (join ("\t", ($ldigits, $tdigits)));
		if ( exists $scaffold{$num} ) { #if hash with that Lscaffold number already exists; to extend scaffold
	#		$count++ ; #add another one to count to count more genes
	#		$scaffold{$num}{1} =( $scaffold{$num}{1} + 1); # reassigns number of genes to add onemore
			if ($line[3] < (2* ($scaffold{$num:$count}{3} ))) {
				$count++;
			}
			if ($line[2]<($scaffold{$num:$count}{2} )) { #if where this gene starts is before the other gene
				$scaffold{$num}{2} = $line[2]; #reassign start of gene to smaller bp location
			}
			if ($line[3]>($scaffold{$num:$count}{3} )) { #if where this gene ends is after the other gene
				$scaffold{$num}{3} = $line[3];#reassign end of gene to larger bp location
			}
			if ($line[5]<($scaffold{$num:$count}{4} )) {#if where this gene starts is before the other gene
				$scaffold{$num}{4} = $line[5];#reassign start of gene to smaller bp location
			}
			if ($line[6]>($scaffold{$num:$count}{5} )) {#if where this gene ends is after the other gene
				$scaffold{$num}{5} = $line[6];#reassign end of gene to larger bp location
			}
			@pacids = ($scaffold{$num:$count}{6}, $line[0] );
		#	$scaffold{$num}{6}, $line[0]; #if more than one pacid, add onto array
			$scaffold{$num$count}{6}= (join ' ', @pacids); #assign array of pacids to hash
			$scaffold{$num$count}{1} =( $scaffold{$num:$count}{1} + 1); # reassigns number of genes to add onemore
		} else { #first Lscaffold of that number
			$countg=1;
			$count=1; #first gene - only one so far
			$scaffold{$num}{$count}{1} = ($count); #genes that match them 
			$scaffold{$num}$count}{2} = ($line[2]); #where it starts on Tropicalis
			$scaffold{$num$count}{3} = ($line[3]); #where it ends on Tropicalis
			$scaffold{$num$count}{4} = ($line[5]); #where it starts on Laevis
			$scaffold{$num$count}{5} = ($line[6]); #where it ends on Laevis
			@pacid=();
			$pacid[0] = ($line[0]); #store pacids into an array
#			$scaffold{$num}{6} = (@pacid); #assign pacid to hash (not array, only scalar)
			$scaffold{$num$count}{6} =  $line[0];
		}	
}
@info = ('#genes:', 'tstart:', 'tstop:', 'lstart:', 'lstop:' ,'pacids:'); #assigned titles to each column to clarify
open (COLLAPSE, ">collapset2") or die "Couldn't open: $!" ; #open file to print to
foreach $Laevis ( keys %scaffold) { #for each Laevis scaffold number
	print COLLAPSE ("$Laevis \t"); #print the Laevis scaffold number
	for ($a=1; $a<7; $a=$a+1) { #go through up to 6 (total number of information)
	#foreach $info ( keys % { $scaffold{$num} } ) {
		#print COLLAPSE ($info[($a-1)]); #prints title if wanted it
		print COLLAPSE ($scaffold{$Laevis}{$a}) ; #print the collapsed data, one item at a time
		print COLLAPSE "\t";
	}
	print COLLAPSE "\n";
}
	
close (COLLAPSE);
close (LINKED);