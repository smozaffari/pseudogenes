#!/usr/bin/env perl

use strict;
use warnings;

my @line;
my $line;
my $hit;
my $gene;
my @info;
my $count;

open (BLAST, "ncRNA.RepeatAligned.txt") or die "Cannot open: $! \n";
open (HITS, ">XHITS.txt") or die "$! \n";
$count=0;
while ($line = <BLAST>) { #read one line at a time
	@line=split(/\t/, $line); # split each word by tabs of each line into an element of an array
	if ($line[2]=~/X/) {
	chomp $line[4];
		if (!$count) { #will be 0 the first time, will not go through till second time.
			$count++; #add one to the count
			$info[0]=$line[0]; #save the new read line as the array to compare new ones to.
			$info[1]=$line[1];
			$info[2]=$line[2];
			$info[3]=$line[3];
			$info[4]=$line[4];
			chomp $info[4];
			
		} else {
			if ($line[0] eq $info[0]) { #if the gene is the same gene
				if ($line[4] > $info[4]) { #and the score of the second one is larger than the first one
					$info[0]=$line[0]; #save the new read line as the array to compare new ones to.
					$info[1]=$line[1];
					$info[2]=$line[2];
					$info[3]=$line[3];
					$info[4]=$line[4];
					chomp $info[4];
				} else {
					if ($line[4] == $info[4]) {
						if ($line[3] > $info[3]) {
							$info[0]=$line[0]; #save the new read line as the array to compare new ones to.
							$info[1]=$line[1];
							$info[2]=$line[2];
							$info[3]=$line[3];
							$info[4]=$line[4];
						}
					}
				}
			} else { #if the gene is a different one
				print HITS (join "\t",(@info)); #print the last gene onto new file
				print HITS "\n"; 
				$info[0]=$line[0]; #save the new read line as the array to compare new ones to.
				$info[1]=$line[1];
				$info[2]=$line[2];
				$info[3]=$line[3];
				$info[4]=$line[4];
				chomp $info[4];
			}
		}
	}
}


close(BLAST);
close(HITS);
		