#!/usr/bin/env perl

use strict;
use warnings;
# my $numargs = @ARGV;


open (COLLAPSET, "12313.txt" ) or die "Cannot open: $! "; 
	# file sorted by Tscaffold number and by Trop start location
open (TABLE, ">table") or die "Cannot open: $! ";
print TABLE (" Left Scaffold  Lstart  Lstop  Tstart  stop \n");

my $count=0;
my $Atscaff;
my $Atstart;
my $Atstop;
my $Alscaff;
my $Amargin;

my $line;
my @line;

my $Btscaff;
my $Btstart;
my $Btstop;
my $Blscaff;
my $Bmargin;

my $Ctscaff;
my $Ctstart;
my $Ctstop;
my $Clscaff;

my @distance;
my %info;
my %chart;
my $Left;
my $Right;
my $Splint;
my $wrong=0;
=head
sub find_left {
	if ($Btstop < $Atstop) {
		@distance= ($Btstop, $Atstop);
		$Left = $Blscaff;
		$chart{$Left}= ($info{$Blscaff});
		$Ctscaff = $Btscaff;
		$Ctstart = $Btstart;
		$Ctstop = $Btstop;
		$Clscaff = $Clscaff;	
		$info{$Clscaff}= ($info{$Blscaff});	
	} else {
		if ($Atstop < $Btstop) {
			@distance = ($Atstop, $Btstop);
			$Left = $Alscaff;
			$chart{$Left} = ($info{Alscaff});
			print (" $Left, $Atstop, $Btstop, \t");
			$Ctscaff = $Atscaff;
			$Ctstart = $Atstart;
			$Ctstop = $Atstop;
			$Clscaff = $Alscaff;	
			$info{$Clscaff}= ($info{$Alscaff});	
		} else {		
			$distance[0]=0;
			$distance[1]=0;
		}
	}
}
=cut


while ($line = <COLLAPSET>) {
	@line=split(/\t/, $line);	
	if ($count == 0) {
		$Atscaff = $line[1];
		$Atstart = $line[3];
		$Atstop = $line[4];
		$Alscaff = $line[0];
		$Amargin=($line[4]-$line[3]);
		my $Alsize=($line[6]-$line[5]);
			if ($Alsize < $Amargin) {
		 		print ("wrong match $Alscaff $Atstart - $Atstop true size: $line[4] - $line[3] \n");
		 		next;
		 	}
		$info{$Alscaff}= (" $line[5], $line[6], $line[3], $line[4]");
						# L start,   L stop,  T start,  T stop
		$count++;
	} else {
		if ($count == 1) {
			$Btscaff = $line[1];
			$Btstart = $line[3];
			$Btstop = $line[4];
			$Blscaff = $line[0];
			my $Bmargin = ($line[4]-$line[3]);	
			my $Blsize=($line[6]-$line[5]);
			if ($Blsize < $Bmargin) {
		 		print ("wrong match $Blscaff $Btstart - $Btstop true size: $line[4] - $line[3] \n");
		 		next;
		 	}
			$info{$Blscaff}= (" startL: $line[5] stopL: $line[6], startT: $line[3] stopT: $line[4]" );
							# L start,   L stop,  T start,  T stop
			$count++;
			if ($Btstop < $Atstop) {
				@distance= ($Btstop, $Atstop);
				$Left = $Blscaff;
				$chart{$Left}= ($info{$Blscaff});
				$Splint = $Alscaff;
				$chart{$Splint}=($info{$Alscaff});
				$Ctscaff = $Btscaff;
				$Ctstart = $Btstart;
				$Ctstop = $Btstop;
				$Clscaff = $Blscaff;	
				$info{$Clscaff}= ($info{$Blscaff});	
			} else {
				if ($Atstop < $Btstop) {
					@distance = ($Atstop, $Btstop);
					$Left = $Alscaff;
					$chart{$Left} = ($info{Alscaff});
					$Ctscaff = $Atscaff;
					$Ctstart = $Atstart;
					$Ctstop = $Atstop;
					$Clscaff = $Alscaff;	
					$info{$Clscaff}= ($info{$Alscaff});	
					if ($Btstart > $Atstop) {
						$count=0;
						next;
					}
				} else {		
					@distance=();
					$count=0;
				}
			}
			if ($Clscaff=$Blscaff) {
				$Btscaff = $Atscaff;
				$Btstart = $Atstart;
				$Btstop = $Atstop;
				$Blscaff = $Alscaff;
				$info{$Blscaff}=($info{$Alscaff});
			}
		} else {
			$Atscaff = $line[1];
			$Atstart = $line[3];
			$Atstop = $line[4];
			$Alscaff = $line[0];	
			$info{$Alscaff}= (" startL: $line[5] stopL: $line[6], startT: $line[3] stopT: $line[4]" );
#			if ($Btscaff=$Atscaff) {				# if on the same T scaffold
				if ( ($distance[1]-$distance[0])>1 ) {				# if there is a distance between end of prev 2 scaffolds
					if ($Atstart >= $distance[0] ) {# if the next scaffold starts after where the last one began
						if ($Atstart < $Btstop) {
							$Right = $Alscaff;		    # it is the scaffold on the right of the gap.
							$chart{$Right}= $info{$Alscaff};	# put its info into hash
							$Splint = $Blscaff;
							$chart{$Splint} = $info{$Blscaff};
							print TABLE ( join " ", ("Left", $Left, $chart{$Left}, "Right", $Right, $chart{$Right}, $Btscaff, "Splint", $Blscaff, $Btstart, $Btstop) );
							print TABLE ("\n");
							# print hash with L scaffold info, Right scaffold info, T scaffold # , L splint, Splint position
							if ($Atstop > $Btstop) {	
								@distance= ($Btstop, $Atstop);
								$Left = $Blscaff;
								$chart{$Left}= ($info{$Blscaff});
							} else {
								if ($Atstop = $Btstop) {
									$distance[1]=0;
									$distance[0]=0;
									$count=0;
								}
							}
						} else {
							if ($Btstop < $Atstop) {
								@distance= ($Btstop, $Atstop);
								$Left = $Blscaff;
								$chart{$Left}= ($info{$Blscaff});
								$Ctscaff = $Btscaff;
								$Ctstart = $Btstart;
								$Ctstop = $Btstop;
								$Clscaff = $Clscaff;	
								$info{$Clscaff}= ($info{$Blscaff});	
							} else {
								if ($Atstop < $Btstop) {
									@distance = ($Atstop, $Btstop);
									$Left = $Alscaff;
									$chart{$Left} = ($info{Alscaff});
						#			print TABLE ( join " ", ("Left", $Left, $chart{$Left}, "Right", $Right, $chart{$Right}, $Btscaff, "Splint", $Blscaff, $Btstart, $Btstop) );
						#			print TABLE ("\n");
									$Ctscaff = $Atscaff;
									$Ctstart = $Atstart;
									$Ctstop = $Atstop;
									$Clscaff = $Alscaff;	
									$info{$Clscaff}= ($info{$Alscaff});	
								} else {		
									@distance=();
								}
							}
						}
					}
				} 
			}
		}
	}


close (COLLAPSET);
close (TABLE);