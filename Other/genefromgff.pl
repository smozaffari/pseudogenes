#!/usr/bin/env perl

use strict;
use warnings;
my $pacID;
my $count=0;
my $min =-1;
my $max =-1;
my %lx;
my $line;
my @line;
my $scaffold;
my $xline;
my @xline;
my %final;
my $direction;

my $numargs = @ARGV; # gff file with gene and mrna, file to write to
if ($numargs != 2) { 						# makes sure 2 files were used as arguments
    print "\not enough files\n";			
    exit;									
}
open (LSCAFFOLD, "$ARGV[0]") or die "Cannot open: $! \n";
open (LXSUM, ">$ARGV[1]") or die "$! \n";
while ($line = <LSCAFFOLD>) { #read one line at a time
	$line =~ /#/ && next; #if it has a # go on to next line (common with gff files)
	@line=split(/\t/, $line); # split each word by tabs of each line into an element of an array

	if ($line[2]=~/mRNA/) { #if it is mRNA
		($pacID)=$line[8]=~/([0-9]{8})/; #take out the pacid number
		$scaffold=$line[0]; #keep the scaffold number
		$min=$line[3];
		$max=$line[4];
		$lx{$pacID} = ( "$scaffold $min $max "); #assign values/info to hash with key being pacidnumber
	}
}

foreach ( sort keys %lx) { #sort keys and for each key print pacid number and each collapsed info of scaffold with tab between
	print LXSUM (join "\t", ($_, $lx{$_}));
	print LXSUM "\n";
}
=head
open (XTR, "xtr_sorted") or die "Cannot open xtr: $!";
open (FINAL, ">$ARGV[3]") or die "Cannot open final: $!";
while ($xline = <XTR>) { #read in file one line at a time
	@xline = split(/\t/, $xline); #split line into elements of an array
	$final{$xline[0]}= ("$xline[1] $xline[4] $xline[5] $xline[6] $lx{$xline[0]} "); #took out important values and assigned to each pacid number
	print FINAL (join "\t", ("$xline[0]", $xline[1], $xline[4], $xline[5], $lx{$xline[0]},"\n")  );
	#order by trop scaffold number ^pacid# ^Tscaff# ^Trop: start and stop  ^Laevis start and stop 
}
close(XTR);
close(FINAL);
=cut

close(LSCAFFOLD);
close(LXSUM);
