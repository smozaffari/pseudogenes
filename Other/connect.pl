#!/usr/bin/env perl

# use strict;
use warnings;

my $A =0; 
my @invalid = ();
my $start = 37126;
my $end = 204912;

open (FILE, "sorted100") or die "Cannot open: $!" ;
open (RESULT, ">results") or die "$! \n";
open (SIZE, "L6.1.scaf_sizes") or die "Cannot open: $!" ;

while (my $line = <FILE>) {
	chomp $line;
	my @line=split(/\t/, $line);
	$A++;
	$info{$A}=[ $line[0], $line[3], $line[4] ];
}

while (my $line = <SIZE>) {
	chomp $line;
	my @line=split(/\t/, $line);
	$B= $line[0];
	$size{$B}=$line[1];
}

$start = $info{1}[1];
$end = $info{1}[2];

foreach $l (keys %info) {
	
	$scc= $info{$l}[0];
	$scsize= $size{$scc};
	if (($info{$l}[2])/($info{$l}[1]) > $scsize ) {
		push (@invalid, $info{$l}[0]);	
	} else {
		
		if (($info{$l}[1] > $start ) and ($info{$l}[2] > $end)) {
			$end = ($info{$l}[2]);
		} else {
			print RESULT (" $info{$l}[0] $start $end \n");
			$start = ($info{$l}[1]);
		}
	}
}

close (FILE);
close (RESULT);
close (SIZE);