#!/usr/bin/perl -w

$im_thinking_of=int(rand 10);
print "Pick a number between 0 and 10:";
$guess=<STDIN>;
chomp $guess; # Don't forget to remove the newline!

while ( $guess != $im_thinking_of ) {

	if ($guess > $im_thinking_of) {
		print "You guessed too high!\n";
	} 
	
	if ($guess < $im_thinking_of) {
		print "You guessed too low!\n";
	} 
			
	print "Try again:";
	$guess=<STDIN>;
	chomp $guess;	
}	

print "You got it right!\n";