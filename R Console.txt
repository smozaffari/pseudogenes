
R version 2.15.1 (2012-06-22) -- "Roasted Marshmallows"
Copyright (C) 2012 The R Foundation for Statistical Computing
ISBN 3-900051-07-0
Platform: x86_64-apple-darwin9.8.0/x86_64 (64-bit)

R is free software and comes with ABSOLUTELY NO WARRANTY.
You are welcome to redistribute it under certain conditions.
Type 'license()' or 'licence()' for distribution details.

  Natural language support but running in an English locale

R is a collaborative project with many contributors.
Type 'contributors()' for more information and
'citation()' on how to cite R or R packages in publications.

Type 'demo()' for some demos, 'help()' for on-line help, or
'help.start()' for an HTML browser interface to help.
Type 'q()' to quit R.

[R.app GUI 1.52 (6188) x86_64-apple-darwin9.8.0]

[History restored from /Users/saharmozaffari/.Rapp.history]

> KaKs <- read.table ("KaKs")
> plot (x=KaKs$V3)
> hist (x=KaKs$V4)
> plot (x=KaKs$V4, y=KaKs$V5)
> plot (x=KaKs$V4, y=KaKs$V5, xlim=4)
Error in plot.window(...) : invalid 'xlim' value
> plot (x=KaKs$V4, y=KaKs$V5, xlim=4 )
Error in plot.window(...) : invalid 'xlim' value
> plot (x=KaKs$V4, y=KaKs$V5, xlim=(0,4) )
Error: unexpected ',' in "plot (x=KaKs$V4, y=KaKs$V5, xlim=(0,"
> plot (x=KaKs$V4, y=KaKs$V5, xlim=0,4 )
Error in plot.window(...) : invalid 'xlim' value
> plot (x=KaKs$V4, y=KaKs$V5)
> plot (x=KaKs$V7, y=KaKs$V8)
> plot (x=KaKs$V3, y=KaKs$V2)
> plot (x=KaKs$V4, y=KaKs$V5, xlim= 0,4 )
Error in plot.window(...) : invalid 'xlim' value
> plot (x=KaKs$V4, y=KaKs$V5, xlim= (0,4) )
Error: unexpected ',' in "plot (x=KaKs$V4, y=KaKs$V5, xlim= (0,"
> plot (x=KaKs$V4, y=KaKs$V5, xlim= (0 4) )
Error: unexpected numeric constant in "plot (x=KaKs$V4, y=KaKs$V5, xlim= (0 4"
> plot (x=KaKs$V4, y=KaKs$V5, xlim= 0 4 )
Error: unexpected numeric constant in "plot (x=KaKs$V4, y=KaKs$V5, xlim= 0 4"
> plot (x=KaKs$V4, y=KaKs$V5, xlim= c(0, 4) )
> plot (x=KaKs$V5, y=KaKs$V6, xlim= c(0, 4) )
> plot (x=KaKs$V5, y=KaKs$V6, xlim= c(0, 0.8) )
> plot (x=KaKs$V5, y=KaKs$V6, )
> point (x=0.05858438, y=0.9293965)
Error: could not find function "point"
> points (x=0.05858438, y=0.9293965, col="red")
> points (x=0.05858438, y=0.9293965, col="red", bg = "red")
> points (x=0.05858438, y=0.9293965, col="red", pch=21)
> points (x=0.05858438, y=0.9293965, col="red", pch=20)
> points (x=0.02047148,	y=0.6109328, col="red", pch=20)
> points (x=0.01315414,	y=0.1396813, col="yellow", pch=20)
> points (x=0.01315414,	y=0.1396813, col="lightblue", pch=20)
> points (x=0.01315414,	y=0.1396813, col="orange", pch=20)
> points (x=0.01093435,	y=0.2503181, col="orange", pch=20)
> points (x=0.03874574,	y=0.03570145, col="yellow", pch=20)
> points (x=0.03874574,	y=0.03570145, col="lightblue", pch=20)
> points (x=0.03874574,	y=0.03570145, col="blue", pch=20)
> points (x=0.03874574,	y=0.03570145, col="pink", pch=20)
> points (x=0.03874574,	y=0.03570145, col="magenta", pch=20)
> points (x=0.113694	, y=0.1114834, col="magenta", pch=20)
> points (x=0.1846912	,y=0.01905313, col="blue", pch=20)
> points (x=0.1846912	,y=0.01905313, col="lightblue", pch=20)
> points (x=0.1846912	,y=0.01905313, col="yellow", pch=20)
> points (x=0.1846912	,y=0.01905313, col="purple", pch=20)
> points (x=0.1846912	,y=0.01905313, col="lightblue", pch=20)
> points (x=0.08172838,	y=0.05031959, col="lightblue", pch=20)
> plot (x=KaKs$V5, y=KaKs$V6, xlabel = "Conserved gene Ka", ylabel = "Pseudogene Ka" )
There were 12 warnings (use warnings() to see them)
> plot (x=KaKs$V5, y=KaKs$V6, xlabel = "Conserved gene Ka", ylab = "Pseudogene Ka" )
Warning messages:
1: In plot.window(...) : "xlabel" is not a graphical parameter
2: In plot.xy(xy, type, ...) : "xlabel" is not a graphical parameter
3: In axis(side = side, at = at, labels = labels, ...) :
  "xlabel" is not a graphical parameter
4: In axis(side = side, at = at, labels = labels, ...) :
  "xlabel" is not a graphical parameter
5: In box(...) : "xlabel" is not a graphical parameter
6: In title(...) : "xlabel" is not a graphical parameter
> plot (x=KaKs$V5, y=KaKs$V6, xlab = "Conserved gene Ka", ylab = "Pseudogene Ka" )
> plot (x=KaKs$V5, y=KaKs$V6, xlab = "Conserved gene Ka", ylab = "Pseudogene Ka", main = "Conserved gene Ka vs Pseudogene Ka for Ks < 0.5" )
> points (x=0.05858438, y=0.9293965, col="red", pch=20)
> > points (x=0.02047148,	y=0.6109328, col="red", pch=20)
Error: unexpected '>' in ">"
> > points (x=0.02047148,	y=0.6109328, col="red", pch=20)
Error: unexpected '>' in ">"
> points (x=0.02047148,	y=0.6109328, col="red", pch=20)
> points (x=0.01315414,	y=0.1396813, col="yellow", pch=20)
> points (x=0.01315414,	y=0.1396813, col="magenta", pch=20)
> points (x=0.01093435,	y=0.2503181, col="magenta", pch=20)
> points (x=0.03874574,	y=0.03570145, col="yellow", pch=20)
> points (x=0.113694	, y=0.1114834, col="magenta", pch=20)
> points (x=0.113694	, y=0.1114834, col="yellow", pch=20)
> points (x=0.1846912	,y=0.01905313, col="orange", pch=20)
> points (x=0.5531945	, y=0.306635, col="lightblue", pch=20)
> points (x=0.5531945	, y=0.306635, col="orange", pch=20)
> points (x=0.3032477,	y=0.07972691, col="magenta", pch=20)
> points (x=0.3032477,	y=0.07972691, col="orange", pch=20)
> points (x=0.09007328,	y=0.2221865, col="orange", pch=20)
> points (x=0.09007328,	y=0.2221865, col="magenta", pch=20)
> points (x=0.01093435, y=	0.2503181, col="yellow", pch=20)
> plot (x=KaKs$V5, y=KaKs$V6, xlab = "Conserved gene Ka", ylab = "Pseudogene Ka", main = "Conserved gene Ka vs Pseudogene Ka for Ks < 0.5" )
> points (x=0.05858438, y=0.9293965, col="red", pch=20)
> points (x=0.02047148,	y=0.6109328, col="red", pch=20)
> points (x=0.01093435,	y=0.2503181, col="red", pch=20)
> plot (x=KaKs$V5, y=KaKs$V6, xlab = "Conserved gene Ka", ylab = "Pseudogene Ka", main = "Conserved gene Ka vs Pseudogene Ka for Ks < 0.5" )
> plot (x=KaKs$V5, y=KaKs$V6, xlab = "Conserved gene Ka", ylab = "Pseudogene Ka", main = "Conserved gene Ka vs Pseudogene Ka for Ks < 0.5" )
> points (x=0.05858438, y=0.9293965, col="red", pch=20)
> points (x=0.02047148,	y=0.6109328, col="red", pch=20)
> points (x=0.01315414, y=	0.1396813, col="red", pch=20)
> points (x=0.02047148, y=	0.6109328, col="red", pch=20)
> points (x=0.03874574, y=	0.03570145, col="red", pch=20)
> points (x=0.05858438, y=	0.9293965, col="red", pch=20)
> points (x=0.08172838, y=	0.05031959, col="red", pch=20)
> points (x=0.09007328, y=	0.2221865, col="red", pch=20)
> points (x=0.113694	0.1114834, col="red", pch=20)
Error: unexpected numeric constant in "points (x=0.113694      0.1114834"
> points (x=0.113694, y=	0.1114834, col="red", pch=20)
> points (x=0.08172838, y=	0.05031959, col="white", pch=20)
> points (x=0.1846912, y=	0.01905313, col="white", pch=20)
> points (x=0.1846912, y=	0.01905313, col="red", pch=20)
> points (x=0.3032477, y=	0.07972691, col="red", pch=20)
> points (x=0.5531945, y=	0.306635, col="red", pch=20)
> points (x=0.02047148, y= 0.61 col="red", pch=20)
Error: unexpected symbol in "points (x=0.02047148, y= 0.61 col"
> points (x=0.02047148, y= 0.61, col="yellow", pch=20)
> points (x=0.02047148, y= 0.61, col="red", pch=20)
> points (x=0.05858438, y=0.9293965, col="white", pch=20)
> hist (x=KaKs$V5)
> hist
function (x, ...) 
UseMethod("hist")
<bytecode: 0x10718b778>
<environment: namespace:graphics>
> hist (x=KaKs$V5, xlim = range (0,1))
> hist (x=KaKs$V5, xlim = range (0,1), counts = 0.1)
Warning messages:
1: In title(main = main, sub = sub, xlab = xlab, ylab = ylab, ...) :
  "counts" is not a graphical parameter
2: In axis(1, ...) : "counts" is not a graphical parameter
3: In axis(2, ...) : "counts" is not a graphical parameter
> hist (x=KaKs$V5, xlim = range (0,1), counts = (0 1))
Error: unexpected numeric constant in "hist (x=KaKs$V5, xlim = range (0,1), counts = (0 1"
> hist (x=KaKs$V5, xlim = range (0,1), counts = (0, 1))
Error: unexpected ',' in "hist (x=KaKs$V5, xlim = range (0,1), counts = (0,"
> hist (x=KaKs$V5, xlim = range c(0,1))
Error: unexpected symbol in "hist (x=KaKs$V5, xlim = range c"
> bins=seq(0,1,by=0.5)
> hist (x=KaKs$V5, xlim = range c(bin)
Error: unexpected symbol in "hist (x=KaKs$V5, xlim = range c"
> hist (x=KaKs$V5, breaks = bins
+ )
> bins=seq(0,1,by=0.01)
> hist (x=KaKs$V5, breaks = bins)
> hist (x=KaKs$V2, breaks = bins)
Error in hist.default(x = KaKs$V2, breaks = bins) : 
  some 'x' not counted; maybe 'breaks' do not span range of 'x'
> hist (x=KaKs$V2)
> bins=seq(0,10,by=0.01)
> hist (x=KaKs$V2, breaks = bins)
> hist (x=KaKs$V2, breaks = bins, xlim = 1)
Error in plot.window(xlim, ylim, "") : invalid 'xlim' value
> hist (x=KaKs$V2, breaks = bins, xlim = (0,1))
Error: unexpected ',' in "hist (x=KaKs$V2, breaks = bins, xlim = (0,"
> hist (x=KaKs$V2, breaks = bins, xlim = (0 1))
Error: unexpected numeric constant in "hist (x=KaKs$V2, breaks = bins, xlim = (0 1"
> hist (x=KaKs$V2, breaks = bins, xlim = range(0 1))
Error: unexpected numeric constant in "hist (x=KaKs$V2, breaks = bins, xlim = range(0 1"
> hist (x=KaKs$V2, breaks = bins, xlim = range c(0 1))
Error: unexpected symbol in "hist (x=KaKs$V2, breaks = bins, xlim = range c"
> ?hist
starting httpd help server ... done
> hist (x=KaKs$V2, breaks = bins, xlim = 0 1))
Error: unexpected numeric constant in "hist (x=KaKs$V2, breaks = bins, xlim = 0 1"
> hist (x=KaKs$V2, breaks = bins, xlim = 0 1)
Error: unexpected numeric constant in "hist (x=KaKs$V2, breaks = bins, xlim = 0 1"
> hist (x=KaKs$V2, breaks = bins, xlim = 1)
Error in plot.window(xlim, ylim, "") : invalid 'xlim' value
> hist (x=KaKs$V2, breaks = bins)
> hist (x=KaKs$V2, breaks = bins, xlim = range(1))
> hist (x=KaKs$V3, breaks = bins)
> hist (x=KaKs$V2, breaks = bins, xlim = range(0, 1))
> hist (x=KaKs$V2, breaks = bins, xlim = range(0, 1))
> hist (x=KaKs$V3, breaks = bins, xlim = range(0, 1))
> hist (x=KaKs$V2, breaks = bins, xlim = range(0, 1))
> hist (x=KaKs$V2, breaks = bins, xlim = range(0, 1), xlab = "Conserved gene Ks", ylab = "Frequency", main = "Ks between Conserved Laevis gene and Tropicalis ortholog ")
> hist (x=KaKs$V2, breaks = bins, xlim = range(0, 1), xlab = "Conserved gene Ks", ylab = "Frequency", main = "Ks between conserved Laevis gene and Tropicalis ortholog ")
> hist (x=KaKs$V1, breaks = bins, xlim = range(0, 1), xlab = "Conserved gene Ks", ylab = "Frequency", main = "Ks between conserved Laevis gene and Tropicalis ortholog ")
Error in hist.default(x = KaKs$V1, breaks = bins, xlim = range(0, 1),  : 
  'x' must be numeric
> hist (x=KaKs$V3, breaks = bins, xlim = range(0, 1), xlab = "Conserved gene Ks", ylab = "Frequency", main = "Ks between conserved Laevis gene and Tropicalis ortholog ")
> hist (x=KaKs$V4, breaks = bins, xlim = range(0, 1), xlab = "Conserved gene Ks", ylab = "Frequency", main = "Ks between conserved Laevis gene and Tropicalis ortholog ")
> hist (x=KaKs$V5, breaks = bins, xlim = range(0, 1), xlab = "Conserved gene Ks", ylab = "Frequency", main = "Ks between conserved Laevis gene and Tropicalis ortholog ")
> hist (x=KaKs$V2, breaks = bins, xlim = range(0, 1), xlab = "Conserved gene Ks", ylab = "Frequency", main = "Ks between conserved Laevis gene and Tropicalis ortholog ")
> hist (x=KaKs$V3, breaks = bins, xlim = range(0, 1), xlab = "Conserved gene Ks", ylab = "Frequency", main = "Ks between conserved Laevis gene and Tropicalis ortholog ")
> hist (x=KaKs$V4, breaks = bins, xlim = range(0, 1), xlab = "Conserved gene Ks", ylab = "Frequency", main = "Ks between conserved Laevis gene and Tropicalis ortholog ")
> hist (x=KaKs$V5, breaks = bins, xlim = range(0, 1), xlab = "Conserved gene Ks", ylab = "Frequency", main = "Ks between conserved Laevis gene and Tropicalis ortholog ")
> hist (x=KaKs$V3, breaks = bins, xlim = range(0, 1), xlab = "Conserved gene Ks", ylab = "Frequency", main = "Ks between conserved Laevis gene and Tropicalis ortholog ")
> hist (x=KaKs$V2, breaks = bins, xlim = range(0, 1), xlab = "Conserved gene Ks", ylab = "Frequency", main = "Ks between conserved Laevis gene and Tropicalis ortholog ")
> median(KaKs)
Error in median.default(KaKs) : need numeric data
> median(KaKs$V2)
[1] 0.2823242
> median(KaKs$V3)
[1] 0.5926725
> count (KaKs$V2)
Error: could not find function "count"
> count(KaKs$V2)
Error: could not find function "count"
> freq(KaKs$V2)
Error: could not find function "freq"
> conserved <- read.table(KaKsconserved)
Error in read.table(KaKsconserved) : object 'KaKsconserved' not found
> conserved <- read.table("KaKsconserved")
> hist (x=KaKsconserved$V2, breaks = bins, xlim = range(0, 1))
Error in hist(x = KaKsconserved$V2, breaks = bins, xlim = range(0, 1)) : 
  object 'KaKsconserved' not found
> hist (x=conserved$V2, breaks = bins, xlim = range(0, 1))
> hist (x=conserved$V3, breaks = bins, xlim = range(0, 1))
> hist (x=conserved$V1, breaks = bins, xlim = range(0, 1))
Error in hist.default(x = conserved$V1, breaks = bins, xlim = range(0,  : 
  'x' must be numeric
> hist (x=conserved$V2, breaks = bins, xlim = range(0, 1))
> median(conserved$V3)
[1] NA
> median(conserved$V2)
[1] 0.263054
> median(conserved$V3)
[1] NA
> mean(conserved$V3)
[1] NA
> hist (x=conserved$V3, breaks = bins, xlim = range(0, 1))
> median (conserved$V3)
[1] NA
> hist (x=conserved$V3)
> hist (x=conserved$V3, breaks = bins, xlim = range(0, 1))
> cons <- read.table(cons)
Error in read.table(cons) : object 'cons' not found
> cons <- read.table("cons")
Error in file(file, "rt") : cannot open the connection
In addition: Warning message:
In file(file, "rt") : cannot open file 'cons': No such file or directory
> 