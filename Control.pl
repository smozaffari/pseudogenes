#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my @line;
my @id;
my %exonstart;
my %exonstop;
my %exon;
my @list;
my $list;
my %pacid;
my %query;
my %start;
my %stop;
my @blastdbcmdnt; #blast data for blastdbcmd on genome for nucleotides of pseudogene
my @blastdbcmdprot; #blast data for blastdbcmd on proteome for protein sequence for paralog of pseudogene
my $i=1;
my $v=1;
my @exonerate;
my $newstart;
my $newend;
my %Laevisname;
my @two;
my $Laevis;
my $var;
my @temp;
my $pseudo;
my @pseudo;
my %pacids;
my $temp2;
my %first;
my %second;
my $gene;
my $save;
my @save;
my @gene;
my @name;
=head
open (IN, "Besthit2000") || die "nope\n";
open (OUT, ">Duplicateexpressed") || die "nope\n";
while ($list = <IN>) { 		#reads in the filed with pseudogenes, sorted and added 500 bp on either side / on one scaffold. 
		@list=split(/\|/, $list);
		$gene = $list[0];
		if ($save) {
	
			if ($gene =~ $save) {
				print OUT (join "\|", @save);
				print OUT (join "\|", @list);
			}
			$save=0;
		}
		$save=$gene;
		@save=@list;

}
  

close (IN);
close (OUT);
=cut
open (FILE, "Duplicateexpressed1000") || die "nope\n"; 

while ($line = <FILE>) { 		#reads in the filed with pseudogenes, sorted and added 500 bp on either side / on one scaffold. 
	@line=split(/\t/, $line);
	@name=split(/\|/, $line[0]);
	$Laevisname{$i} = (join "\|", $name[0], $name[1]);
	$pacid{$i}=$line[1];
	$query{$i}=$line[3];
	$start{$i}=$line[4];
	$stop{$i}=$line[5];
	$i++;
}
close (FILE);
	
while ($v < $i)  { 		#for each line in the array
 #   $pacid = $list[0]; 		#the first column is the pacid that the pseudogene is expected to align most closely to
  #  $query = $list[1]; 		#the second column is the Scaffold on which that pseudogene can be found
   # $start = $list[4]; 		#the location 500bp before where the pseudogene begins aligning to its paralog
   # $stop = $list[5];  		#the location 500bp after where the pseudogene ends aligning to its paralog
#	$var = $Laevisname{$v};
	if ($v/2) {
	#	print ("$pacid $query $start");
		my $num = $v%2;
			print ("$num $query{$v} $start{$v} $stop{$v} $pacid{$v} $Laevisname{$v}\n");
		  # ($query, $start, $stop) = split(/\t/, $v);
		  # push (@blastdbcmd, `blastdbcmd -dbtype nucl -db /house/homedirs/a/amsession/Xenopus/full_repeats.fa -entry $query`);
		if ($num) {
			$newstart = ($start{$v} -500);
			$newend = ($stop{$v} +500);
	
			push (@blastdbcmdnt, `blastdbcmd -db ~/Laevis/Genome/LAEVIS_6.1.repeatMasked.fa -dbtype nucl -entry $query{$v} -range $newstart-$newend -out "./LA1/$pacid{$v}.nt"`);
			#runs blastdcmd on Laevis genome finding location 500 bp +- where pseudogene is found
			
			push (@blastdbcmdprot, `blastdbcmd -db ~/Sahar_safe/Data/Xtr_72_longest_pep.fa -dbtype prot -entry 'lcl|$pacid{$v}' -out "./LA1/$pacid{$v}.prot"`);
			#runs blastdcmd on Tropicalis proteome finding protein sequence for the paralog of pseudogene
		
			push (@exonerate, `exonerate --exhaustive y --showalignment True --showvulgar False -S False -m protein2genome:bestfit ./LA1/$pacid{$v}.prot ./LA1/$pacid{$v}.nt > ./LaevisexonerateOne/$pacid{$v}`);
			# --ryo ">%ti (%tcb - %tce)\n%tcs\n"   
		} else {
			$newstart = ($start{$v} -500);
			$newend = ($stop{$v} +500);
	
			push (@blastdbcmdnt, `blastdbcmd -db ~/Laevis/Genome/LAEVIS_6.1.repeatMasked.fa -dbtype nucl -entry $query{$v} -range $newstart-$newend -out "./LA2/$pacid{$v}.nt"`);
			#runs blastdcmd on Laevis genome finding location 500 bp +- where pseudogene is found
			
			push (@blastdbcmdprot, `blastdbcmd -db ~/Sahar_safe/Data/Xtr_72_longest_pep.fa -dbtype prot -entry 'lcl|$pacid{$v}' -out "./LA2/$pacid{$v}.prot"`);
			#runs blastdcmd on Tropicalis proteome finding protein sequence for the paralog of pseudogene
		
			push (@exonerate, `exonerate --exhaustive y --showalignment True --showvulgar False -S False -m protein2genome:bestfit ./LA2/$pacid{$v}.prot ./LA2/$pacid{$v}.nt > ./LaevisexonerateTwo/$pacid{$v}`);
			# --ryo ">%ti (%tcb - %tce)\n%tcs\n"   
		}
			# to get the dna sequence that aligns to protein w/o splice sites or introns or frameshifts. 

		#CHANGED: to show vulgar output to parse for insertions/deletions
		$v++;
	}
}

foreach my $i (@blastdbcmdnt) {
    print "$i\n";
}
