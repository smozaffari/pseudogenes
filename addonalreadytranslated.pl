#!/usr/bin/env perl

=head

4/1/13: rerun pseudoTrLbcds on ssh. will rename all cds from pseudo genes in middleprot folder
then will use file names to get to corresponding file names in different folders.
due 4/3/13
=cut

use strict;
use warnings;
my @line;
my $line;
my $lines;
my @lines;
my $lined;
my @lined;
my @files;
my $file;
# my $name;
my %key;
my @transeq;

open (IN, "headTriple") || die "nope3\n";
while ($line=<IN>) {
	@lines=split(/\t/, $line);
	chomp $lines[2];
	print ("$lines[1] $lines[2]\t");
	$key{$lines[1]}= $lines[2];
#	print OUT ("$lines[0]\t$lines[1]\t$lines[2]\n");
}

close (IN);

push (@transeq, `transeq ./cleanedpseudocds/$file ./translatedpseudocds/$file`);
	#translates pseudogenes saves in folder.
	
opendir (RH, "./translatedpseudocds") or die "Cannot open: $! \n";

while ($file=readdir RH) {
	open (TRANSLATED, "./translatedpseudocds/$file") or die "nope4: $! \n";
	open (ADDON, "./proteinsLT/$file.prot"); #attaches translated pseudogene onto proteinfile.
	print (" $key{$file} $file \t");
#	print ADDON (" $key{$file}");
	while ($lined=<TRANSLATED>) { #read one line at a time
		$lined =~ /\d+/ && next; #if it has a # go on to next line (common with gff files)
	} else {
		print 
#	print ADDON ($lined);
}

close (ADDON);
close (TRANSLATED);
closedir (RH);