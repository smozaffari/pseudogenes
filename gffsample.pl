#!/usr/bin/env perl

#use strict;
use warnings;

#pacid: insertion: length; exon # / total exons

my @line;
my $line;
my @id;
my %exonstart;
my %exonstop;
my %exon;
my $intron=0;
my @intron;
my $length;
my $exon;
my $size;
my $insertion;
my $deletion;
my $id;
my $info;
my @name2;
my @gene;
my $var=0;


open (DEL, ">DELETIONS") or die "$! \n";
open (INS, ">INSERTIONS") or die "$! \n";
open (FRS, ">FRAMESHIFTS") or die "$! \n";

print DEL ( "Trop PACID\t exon#\t size\t exon\n");
print INS ( "Trop PACID\t exon#\t size\t exon\n");
print FRS ( "Trop PACID\t exon#\t size\t exon\n");

open (GFF, "./output/pseudogffs") or die "Cannot open: $! \n";
open (FILE, ">SAMPLEGFF") or die "$! \n";
while ($line = <GFF>) { #read one line at a time
	$line =~ /#/ && next; #if it has a # go on to next line (common with gff files)
	if ($line =~ /Command/) {
		$var=1;
	}
	$line =~ /Hostname/ && next;
	$line =~ /-- / && next;
	@line=split(/\t/, $line); # split each word by tabs of each line into an element of an array

	if (($line[2])=~ /gene/) { #if the piece is a gene/ says gene

			@name =split(/\;/, $line[8]);
			@name2 =split(/\|/, $name[1]);
			$info = ($name2[1]);

	}
	if (($line[2])=~ /exon/) {
		print $info;
		($exonstart{$info}{$var}) = $line[3]; #record start by pacID number
		($exonstop{$info}{$var})= $line[4]; #record stop by pacID number
		($exon{$info}{$var})=($line[4]-$line[3]+1);
	#	print FILE ("$info \t $var\t $exon{$info}{$var}\n");
	
		if (($line[8])=~ /insertions (\d+) ; deletions (\d+)/) {
			print INS ("$info \t $var\t $1 $exon{$info}{$var}\n");
			print DEL ("$info \t $var\t $2 $exon{$info}{$var}\n");
			if (($line[8])=~ /frameshifts (\d+)/) {
				print FRS ("$info \t $var\t $1 $exon{$info}{$var}\n");
			}
		}
		$var++;
	} 

}
close(FILE);
close(GFF);
close(INS);
close(DEL);
close(FRS);