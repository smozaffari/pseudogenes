#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my @line;
my $newstart=0;
my $newend =1;
my %query;
my %start;
my %stop;
my $num;
my %info;
my $counter=1;

open (IN, "Alocations.txt") || die "nope\n";
open (NEW, ">Locations") || die "nope2\n";

while ($line = <IN>) { 		 
	@line=split(/\t/, $line);
	print ("$counter @line \t");
	if (($info{$counter}) = ($line[0])) {
		$stop{$counter} = $line[3];
	} else {
		if ($counter == 1) {
			$info{$counter} = $line[0];
			$start{$counter} = $line[2];
			$stop{$counter} = $line[3];
			$query{$counter} = $line[1];
			$counter++;
		} else {
			print ( ($query{$counter},"\t", $start{$counter},"\t", $stop{$counter}, "\n"));
			$counter++;
			$info{$counter} = $line[3];
			$start{$counter} = $line[2];
			$stop{$counter} = $line[3];
			$query{$counter} = $line[1];
			
		}
	}
}

close(IN);
close (NEW);