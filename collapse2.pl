#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my @line;
my $newstart=0;
my $newend =1;
my %query;
my %start;
my %stop;
my $num;
my %info;
my $count=1; 
my $oldone;
my %scaffold;


open (IN, "Alocations.txt") || die "nope\n";
open (NEW, ">LocationsA") || die "nope2\n";

while ($line = <IN>) { 		 
	@line=split(/\t/, $line);
	$num = $line[0];
	if ($count == 1) {
		$oldone=$num;
		$scaffold{$num}{"lt"} = ($line[1]);
		$scaffold{$num}{"start"} = ($line[2]);
		$scaffold{$num}{"stop"} = ($line[3]);
	}
	if ( exists $scaffold{$num} ) {
		$count++ ;
	#	($scaffold{$num}{'genes'}) = ($count);
		if (($line[3])>($scaffold{$num}{'stop'} ) ){
			(($scaffold{$num}{'stop'} )= $line[3]);
		}
	} else {
		print NEW ("$oldone $scaffold{$oldone}{'lt'}\t$scaffold{$oldone}{'start'}\t$scaffold{$oldone}{'stop'}\n");
	#	$count=1;
		$scaffold{$num}{"lt"} = ($line[1]);
		$scaffold{$num}{"start"} = ($line[2]);
		$scaffold{$num}{"stop"} = ($line[3]);
		$oldone= $num;
	}
}

close(IN);
close (NEW);