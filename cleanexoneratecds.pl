#!/usr/bin/env perl

use strict;
use warnings;
my $line;
my @line;
my @files;
my $file;
my $name;


opendir(DH, "./cdsexonerate") or die "Cannot open: $! \n";

while ($file=readdir DH) {
	open (FILEH, "./cdsexonerate/$file") or die "nope1: $! \n";
	open (OUT, " > ./cdsexonerate2/$file.fasta") or die "nope2: $! \n";


while ($line = <FILEH>) { #read one line at a time
		$line =~ /Command/ && next; #if it has a # go on to next line (common with gff files)
		$line =~ /%tcs/ && next;
		$line =~ / False/ && next;
		$line =~ /Hostname/ && next;
		$line =~ /completed/ && next;
		$line =~ /Data/ && next;
		if ($line =~ /\(/ ) {
			@line=split(/ /, $line);
			print OUT ("$line[0]\n");
		} else {
			print OUT ($line);
		}
	}
	close (FILEH);
	close (OUT);
}
closedir (DH);
