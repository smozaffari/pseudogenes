#!/usr/bin/env perl

use strict;
use warnings;
my $pacID;
my $count=0;
my $min =-1;
my $max =-1;
my %lx;
my $line;
my @line;
my $scaffold;
my $xline;
my @xline;
my %final;
my $direction;
my @blastdbcmdnt;
my $exon;


open (LSCAFFOLD, "sample1") or die "Cannot open: $! \n";
open (LXSUM, ">exonregions") or die "$! \n";
while ($line = <LSCAFFOLD>) { #read one line at a time
	$line =~ /#/ && next; #if it has a # go on to next line (common with gff files)
	@line=split(/\t/, $line); # split each word by tabs of each line into an element of an array
	if ($line[2]=~/gene/) { #if the piece is a gene/ says gene
		if (!$count) { #will be 0 the first time, will not go through till second time.
			$count++; #add one to the count
			next;
		} else { # go through this loop the first time
			$lx{$pacID} = ( join "\t",($scaffold , $min, $max )); #assign info to pacid number
			$min=-1; #record info once you hit next gene
			$max=-1; #set all variables back to 0.
		}
	} 
	if ($line[2]=~/mRNA/) { #if it is mRNA
		($pacID)=$line[8]=~/([0-9]{8})/; #take out the pacid number
		$exon =1;
	}
	if ($line[2]=~/CDS/) { #if coding sequence
		
		$scaffold=$line[0]; #keep the scaffold number
		@line=split(/\t/, $line);
		
		if ($min<0) { #assign value to minimum, min is always less than 0 first time, so first piece always min
			$min=$line[3];
		}
		if ($line[3]< $min) { #if another piece is smaller than min, assign that to min
			$min=$line[3];
			$direction = "-"; #if Lscaffold is decreasing as trop is increasing (is) direction is negative
		} else {
			$direction="+";
		}
		if ($line[4]> $max) { #record maximum/end of coding sequence
			$max =$line[4];
		}	
	#	if ($line[4]>=$line[3]) { #if not decreasing, it's increasing.
	#		$direction="+";
	#	}
		print LXSUM ("$scaffold $line[3] $line[4] $pacID $exon $direction \n");
		my $var = ("$pacID:$exon");
		open (FH, ">./Laevisexons/$var") or die "Cannot open2: $! \n";
		print FH ("$pacID:$exon\n");
		close (FH);
		push (@blastdbcmdnt, `blastdbcmd -db ~/Laevis/Genome/LAEVIS_7.1.repeatMasked.fa -dbtype nucl -entry $scaffold -range $line[3]-$line[4] -outfmt %s >> "./Laevisexons/$var"`);
		$exon++;

	}
}
#$lx{$pacID} = ( "$scaffold $min $max $direction"); #assign values/info to hash with key being pacidnumber
#foreach ( sort keys %lx) { #sort keys and for each key print pacid number and each collapsed info of scaffold with tab between
#	print LXSUM (join "\t", ($_, $lx{$_}));
#	print LXSUM "\n";
#}
=head
this is for laevis and tropicalis matching.

open (XTR, "xtr_sorted") or die "Cannot open xtr: $!";
open (FINAL, ">final") or die "Cannot open final: $!";
while ($xline = <XTR>) { #read in file one line at a time
	@xline = split(/\t/, $xline); #split line into elements of an array
	$final{$xline[0]}= ("$xline[1] $xline[4] $xline[5] $xline[6] $lx{$xline[0]} "); #took out important values and assigned to each pacid number
	print FINAL (join "\t", ("$xline[0]", $xline[1], $xline[4], $xline[5], $lx{$xline[0]},"\n")  );
	#order by trop scaffold number ^pacid# ^Tscaff# ^Trop: start and stop  ^Laevis start and stop 
}
close(XTR);
close(FINAL);
=cut

close(LSCAFFOLD);
close(LXSUM);
