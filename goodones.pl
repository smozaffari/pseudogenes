#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my $file;
my $list;
my @line;
my $count;

opendir (DH, "./cdsproteins") or die "Cannot open: $! \n";
open (GOOD, ">./goodones") or die "Cannotopen: $! \n";
while ($file=readdir DH) {
	open (THREE, "./cdsproteins/$file") or die "nope4: $! \n";
	
	$count = 0;
	while ($list = <THREE>) { 
		if ($list =~ m/\d+/) {
			$count ++;
		}
	}
	if ($count == 3) {
		print GOOD ("$file\n");
	}
	close (THREE);
}

closedir (DH);
close (GOOD);