#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my $file;
my $list;
my @line;
my $count;
my @dialign;

open (GOOD, "./goodones") or die "Cannotopen: 2$! \n";

while ($list = <GOOD>) { 	
	chomp $list;
	print $list;
	push (@dialign, `dialign2-2 -nt -fa ./cdsproteins/$list`);
}
close (GOOD);
