#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my @line;
my @id;
my %exonstart;
my %exonstop;
my %exon;
my @list;
my $list;
my %pacid;
my %query;
my %start;
my %stop;
my @blastdbcmdnt; #blast data for blastdbcmd on genome for nucleotides of pseudogene
my @blastdbcmdprot; #blast data for blastdbcmd on genome for protein sequence for paralog of pseudogene
my @blastdbcmdLa; #laevis cdna
my $i=1;
my $v=1;
my @exonerate;
my %Laevisname;
my %Laeviscds;

open (IN, "Key") || die "nope\n"; #use Key file here. short is just first 10. 
while ($list = <IN>) { 		#reads in the filed with pseudogenes, sorted and added 500 bp on either side / on one scaffold. 
	@list=split(/\t/, $list); 
	$pacid{$i}=$list[0]; #Tropicalis pacid 
	$query{$i}=$list[1]; #Scaffold pseudogene is on
	$start{$i}=$list[4]; #start
	$stop{$i}=$list[5];  #stop
	$i++;
}

open (FILE, "./New_data_030613/Besthit.info") || die "nope\n"; 
while ($line = <FILE>) { 
	@line=split(/\t/, $line); #get Laevis gene name
	chomp $line[1];
	my @two = split(/\|/, $line[0]);
	$Laevisname{$line[1]} = join(':', $two[0], $two[1]); #keep both pieces separated by :
	# also saved by Tropicalis pacid (key to hash)
	
	print ("$Laevisname{$line[1]}\t"); #prints so we know it's working
}
close (FILE);
close (IN);


while ($v < $i)  { 		#for each line in the array
	print ("$pacid{$v} $query{$v} $start{$v} $stop{$v} ");
	my $name = $pacid{$v}; #pacid is key to Laevis gene name 
	print ("$name\t $Laevisname{$name}\n");
    push (@blastdbcmdLa, `blastdbcmd -db ~/Sahar_work/New_data_030613/noJGILaevisprotein.fa -dbtype prot -entry "$Laevisname{$name}" -out "./proteinsLT/$name.prot"`);
	#find Laevis protein sequence based on Laevis gene name  
	
	push (@blastdbcmdprot, `blastdbcmd -db ~/Sahar_work/New_data_030613/Xtr_72_longest_pep.fa -dbtype prot -entry 'lcl|$pacid{$v}' >> "./proteinsLT/$name.prot"`);
	#runs blastdcmd on Tropicalis proteome finding protein sequence for the ortholog of pseudogene

	#push (@exonerate, `exonerate --exhaustive y --showalignment False --showvulgar False --ryo ">%ti (%tcb - %tce)\n%tcs\n" -S False -m protein2genome:bestfit ./middle2/$v.prot ./middle2/$v.nt > ./pseudoprot/$name`);
	# --ryo ">%ti (%tcb - %tce)\n%tcs\n"   
	# don't need it here because cds (cdsfile.pl) is being translated and appended on (addontranslatedcds.pl)
	# usually to get the dna sequence that aligns to protein w/o splice sites or introns or frameshifts. 
	
	#--ryo ">%ti (%tab - %tae)\n%tas\n"
	$v++; #add to go through. nt and prot files for exonerate stored as numbers in middle2 folder
}

foreach my $i (@blastdbcmdnt) {
    print "$i\n";
}
