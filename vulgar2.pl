#!/usr/bin/env perl

#use strict;
use warnings;

#pacid: insertion: length; exon # / total exons

my @line;
my $line;
my $intron=0;
my $length;
my $exon;
my $size;
my $insertion;
my $deletion;
my $id;
my $count;

open (IN, "LaevisBvulgar") || die "nope\n";
open (DEL, ">deletions") || die "nope\n";
open (INS, ">insertions") || die "nope\n";
#$line = <IN>;	
while ($line = <IN>) { #read one line at a time
		$line =~ /Command/ && next; #if it has a # go on to next line (common with gff files)
		$line =~ /%tcs/ && next;
		$line =~ / False/ && next;
		$line =~ /Hostname/ && next;
		$line =~ /completed/ &&next;
		#print OUT ($line);
		#print ($line);
		#print $intron;
		@line=split(/I /, $line);
		$intron = @line;
		print $intron;
		$exon = $intron+1;
		
		print ("$line[4]\n ");
		
		if ($line[0] =~ m/lcl\|(\d+)/) {	
			$id = $1;
		}
	
		foreach (my $i =0; $i < $intron; $i++) {
			my $ex = $i+1;
			while ($line[$i] =~ m/ F (\d+) (\d+) /g) {
				$length = $2;
			
				print INS ("$id: insertion: $length: $ex: $intron \n");
				$insertion++;
			}
	
			while ($line[$i] =~ m/ G (\d+) (\d+) /g) {
				$length = $1*3;
				my $j = $2;
				if ($1 =~ /0/) {
					($length = $j);
				}
				print DEL ("$id: deletion: $length: $ex: $intron \n");
				$deletion++;
			}
		}
}

#for (my $i =0; $i < @line; $i++) {

#	if ($line =~ m/F (\d+) (\d+)/) {
#		$intron++;
#		$length = $2;
#		print $length;
#	}
#}

#	@line=split(/\s+/, $line);
#	if ($line =~ m/I\ \d*\ \d*/)  { 
#		$intron++;
#	} 
#	print "$line[0] $line[9]\t";
#	print "$exon $intron \n";
close (IN);