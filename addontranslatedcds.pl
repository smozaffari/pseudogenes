#!/usr/bin/env perl

=head

4/1/13: rerun pseudoTrLbcds on ssh. will rename all cds from pseudo genes in middleprot folder
then will use file names to get to corresponding file names in different folders.
due 4/3/13
=cut

use strict;
use warnings;
my @line;
my $line;
my $lines;
my @lines;
my $lined;
my @lined;
my @files;
my $file;
my $name;
my %key;
my @transeq;
my $liner;
my @liner;
my $linedr;


opendir(DH, "./pseudocds") or die "Cannot open: $! \n";
opendir (LH, "./cleanedcds") or die "Cannot open: $! \n";
opendir (RH, "./translatedpseudocds") or die "Cannot open: $! \n";

while ($file=readdir DH) {
	open (FILEH, "./pseudocds/$file") or die "nope1: $! \n"; 
	open (OUT, ">./cleanedcds/$file.cds") or die "nope2: $! \n";
	while ($line = <FILEH>) { #read one line at a time
		
		$line =~ /Command/ && next; #if it has a # go on to next line (common with gff files)
		$line =~ /%tcs/ && next;
		$line =~ / False/ && next;
		$line =~ /Hostname/ && next;
		$line =~ /completed/ &&next;
		print OUT ($line);
		print ($line);
		
	}
	close (FILEH);
	close (OUT);
	
	push (@transeq, `transeq ./cleanedcds/$file.cds ./translatedpseudocds/$file`);
	#translates pseudogenes saves in folder.
}
closedir (DH);
closedir (LH);


open (IN, "Besthit.triplet") || die "nope3\n";
while ($lines = <IN>) {
	@lines=split(/\t/, $lines);
	chomp $lines[2];
	$key{$lines[1]}= $lines[2];
#	print OUT ("$lines[0]\t$lines[1]\t$lines[2]\n");
}

close (IN);

opendir (RH, "./translatedpseudocds") or die "Cannot open: $! \n";
opendir (LH2, "./cleanedcds") or die "Cannot open: $! \n";
while ($file=readdir RH) {
	open (TRANSLATED, "./translatedpseudocds/$file") or die "nope4: $! \n";
	open (DIRECT, "./cleanedcds/$file.cds") or die "nope4: $! \n";
	open (ADDON, ">>./proteinsLT/$file.prot"); #attaches translated pseudogene onto proteinfile.
	open (ADDON2, ">>./cdsproteins/$file.cds");
	print ADDON2 (">$key{$file}\n");
	print ADDON (">$key{$file}\n");
	while ($lined=<TRANSLATED>) { #read one line at a time
		$lined =~ /\d+/ && next; #if it has a # go on to next line 
		print ADDON ($lined);
	}
	while ($linedr=<DIRECT>) { #read one line at a time
		$linedr =~ /\d+/ && next; #if it has a # go on to next line 
		print ADDON2 ($linedr);
	}

	close (ADDON);
	close (ADDON2);
	close (TRANSLATED);
}
closedir (LH2);
closedir (RH);
