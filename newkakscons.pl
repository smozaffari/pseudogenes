#!/usr/bin/env perl

use strict;
use warnings;
my $pacID;
my $line;
my @line;



open (KAKS, "KaKsconserved") or die "$! \n";
open (NEW, ">KaKsconservedno0") or die "$! \n";
while ($line = <KAKS>) { #read one line at a time
	$line =~ /#/ && next; #if it has a # go on to next line (common with gff files)
	@line=split(/\t/, $line); # split each word by tabs of each line into an element of an array
	print NEW ("$line[0] \t $line[1] \t ");
	if ((! $line[2] ) or (($line[2]) =~ /NA/)) { 
		print NEW ("0.00001 \t");
	} else {
		print NEW ("$line[2] \t");
	}
#	print NEW ("$line[3] \t $line[4] \t $line[5]\t $line[6]\n ");
	if ((! $line[3] ) or (($line[3]) =~ /NA/)) { 
		print NEW ("0.00001 \t");
	} else {
		print NEW ("$line[3] \t");
	}
	if ((! $line[4] ) or (($line[4]) =~ /NA/))  { 
		print NEW ("0.00001 \t");
	} else {
		print NEW ("$line[4] \t");
	}
	if ((! $line[5] ) or (($line[5]) =~ /NA/)) { 
		print NEW ("0.00001 \t");
	} else {
		print NEW ("$line[5] \t");
	}
	if ((! $line[6] ) or (($line[6]) =~ /NA/))  { 
		print NEW ("0.00001 \n");
	} else {
		print NEW ("$line[6] \n");
	}
}

close (KAKS);
close (NEW);