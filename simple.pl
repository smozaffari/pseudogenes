#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my @line;
my $file;
my $count;
my $p;
my $c;
my @protein;
my @cds;
my $save;
my $size;


opendir (DH, "./symbols") or die "Cannot open: $! \n";

while ($file=readdir DH) {
	open (FILEH, "./symbols/$file") or die "nope1: $! \n"; 
	open (FILE2, "> ./clean/$file.cds") or die "nope2: $! \n";
	$count =-1;
	while ($line = <FILEH>) {
		if ($count >= 0) {
			$size = length($line);
			#	print $line[0];
			do {
				$p = substr $save, $count, 1;
				$c = substr $line, $count, 1;
				if ($p =~ /\w/) {
					if ($c=~ /^\./) {
						
					} else {
					#	print $p;
					#	print $c;
						if (($p =~ /[0..9]/) or ($c =~ / /)) {
						} else {
							push @protein, $p;
							push @cds, $c;
						}
					}
				} 
				if ($p =~ /\*/) {
					push @protein, $p;
					push @cds, $c;
				}
				
				$count++;
       		}while (($count) <= ($size));
       	} else {
       	$count++;
       	$save = $line;
    #   	print $save;
       	}
	}
	close (FILEH);	
	print FILE2 (@protein);
	print FILE2 ("\t");
	print FILE2 (@cds);
	close (FILE2);
}