#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my @line;
my @id;
my %exonstart;
my %exonstop;
my %exon;
my @list;
my $list;
my %pacid;
my %query;
my %start;
my %stop;
my @blastdbcmdnt;   # blast data for blastdbcmd on genome for nucleotides of pseudogene
my @blastdbcmdnt2;
my @blastdbcmdprot2;
my @blastdbcmdnt3;
my @blastdbcmdprot; # blast data for blastdbcmd on proteome for protein sequence for paralog of pseudogene
my $i=1;
my $v=1;
my %Laevis;
my %notpseudo;
my @exonerate;
my @exonerate2;
my @exonerate3;
my @exonerate4;
my $newstart;
my $newend;
my $real1;
my $real2;
my @temp;
my $pseudo;
my @pseudo;
my %pacids;
my $temp2;
my %start2; 
my %stop2;
my $newstart2;
my $newend2;
my %query2;
my $temp3;
my %list1;
my %list2;

open (REPEATS, ">samescaffold") || die "nope\n";

open (IN, "norepeats.FLalign.info") || die "nope\n";
while ($list = <IN>) { 		 
	@list=split(/\t/, $list);
	
	$query{$list[0]}=$list[3]; 					# scaffold that Laevis existing gene is on
	$start{$list[0]}=$list[4]; 					# start of Laevis gene
#	chomp $list[5]; 						# removes newline character
	$stop{$list[0]}=$list[5]; 					# stop of Laevis gene
#	$Laevisname{$i}= $list[0]; 				# key = number that the gene is assigned ; hash = the pacid of the Laevisgene
	$pacid{$list[0]} = $list[1];			# key = Laevis ; hash = troppacid
#	$Laevis{$list[1]} = $list[0];           # key = troppacid ; hash = Laevis
	$i++;
}

close (IN);

open (FILE2, "PARA") || die "nope2 $!\n";
while ($pseudo = <FILE2>) { 
	chomp $pseudo;
	@pseudo=split(/\t/, $pseudo); # get Laevis gene name
	$real1 = $pseudo[0];
	$real2= $pseudo[1];
	$list1{$pseudo[0]} = 1;
	$list2{$pseudo[1]} = 1;      # key = pacid #hash = 1 (true)
#	$match{$pseudo[0]} = $pseudo[1];	
=head
}
close (FILE2);
	
while ($v < $i)  { 	              # for each line in the array
	$var = $Laevisname{$v};       # $var = Laevisgene pacid
#	if ($pacid{$var}) {           # if the troppacid for that Laevispacid exists,
		$temp2 = ($pacid{$var});  # assign trop pacid to a temp2 variable
#		if (($query{$v}) eq ($query2{$temp2}) ) {
#			print REPEATS ("$var\n");
#		} else {	
			if ($pacids{$temp2}) {
			   # if it exists in the second hash, it means a corresponding pseudogene exists
=cut
				print ("$real1 $pacid{$real1} $query{$real1} $start{$real1} $stop{$real1}  \n");
			 	# prints all of the information for the existing Laevis gene
	
				$newstart = ($start{$real1});
				$newend = ($stop{$real1});
	
				push (@blastdbcmdnt, `blastdbcmd -db ~/Laevis/Genome/LAEVIS_7.1.repeatMasked.fa -dbtype nucl -entry $query{$real1} -range $newstart-$newend -out "./Data/LaevisC/$real1.nt"`);
				#runs blastdcmd on Laevis genome finding location 500 bp +- where existing Laevis gene is found
			
				push (@blastdbcmdprot, `blastdbcmd -db ~/Sahar_safe/Data/Xtr_72_longest_pep.fa -dbtype prot -entry 'lcl|$pacid{$real1}' -out "./Data/LaevisC/$real1.prot"`);
				#runs blastdcmd on Tropicalis proteome finding protein sequence for the paralog of pseudogene (same thing exists in folder Laevis2)
		
				push (@exonerate, `exonerate --showalignment False --showvulgar False -S False -m protein2genome --ryo ">%ti (%tcb - %tce)\n%tcs\n" ./Data/LaevisC/$real1.prot ./Data/LaevisC/$real1.nt > ./Data/cdsexoneratecons/$pacid{$real1}`);
				# --ryo ">%ti (%tcb - %tce)\n%tcs\n"   
			
				push (@exonerate2, `exonerate --showalignment False --showvulgar True -S False -m protein2genome ./Data/LaevisC/$real1.prot ./Data/LaevisC/$real1.nt > ./Data/vulgarexonerateC/$real1`);
				# --ryo ">%ti (%tcb - %tce)\n%tcs\n"   
			
				push (@blastdbcmdnt3, `blastdbcmd -db ~/Sahar_safe/Data/Xtr72.CDS.fa -dbtype nucl -entry 'lcl|$pacid{$real1}' >> "./Data/cdsexoneratecons/$pacid{$real1}"`);
		
				$newstart2 = ($start{$real2});
				$newend2 = ($stop{$real2});
			
				print ("$real2 $pacid{$real2} $query{$real2} $newstart2 $newend2  \n");
			 	# prints all of the information for the existing Laevis gene

				push (@blastdbcmdnt2, `blastdbcmd -db ~/Laevis/Genome/LAEVIS_7.1.repeatMasked.fa -dbtype nucl -entry $query{$real2} -range $newstart2-$newend2 -out "./Data/LaevisD/$real2.nt"`);
				#runs blastdcmd on Laevis genome finding location 500 bp +- where existing Laevis gene is found
			
				push (@blastdbcmdprot2, `blastdbcmd -db ~/Sahar_safe/Data/Xtr_72_longest_pep.fa -dbtype prot -entry 'lcl|$pacid{$real2}' -out "./Data/LaevisD/$real2.prot"`);
				#runs blastdcmd on Tropicalis proteome finding protein sequence for the paralog of pseudogene (same thing exists in folder Laevis2)
		
				push (@exonerate3, `exonerate --showalignment False --showvulgar False -S False -m protein2genome --ryo ">%ti (%tcb - %tce)\n%tcs\n" ./Data/LaevisD/$real2.prot ./Data/LaevisD/$real2.nt >> ./Data/cdsexoneratecons/$pacid{$real2}`);
				# --ryo ">%ti (%tcb - %tce)\n%tcs\n"  
		
				push (@exonerate4, `exonerate --showalignment False --showvulgar True -S False -m protein2genome ./Data/LaevisD/$real2.prot ./Data/LaevisD/$real2.nt > ./Data/vulgarexonerateD/$real2`);
				# --ryo ">%ti (%tcb - %tce)\n%tcs\n" 
		#	}
  
		#}
	#			# cds = the dna sequence that aligns to protein w/o splice sites or introns or frameshifts. 
	#}
		#CHANGED: to show vulgar output to parse for insertions/deletions
	#	$v++;
}
close (FILE2);
close (REPEATS);
