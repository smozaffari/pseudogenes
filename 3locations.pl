#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my @line;
my @id;
my %exonstart;
my %exonstop;
my %exon;
my @list;
my $list;
my %pacid;
my %query;
my %start;
my %stop;
my @blastdbcmdnt;   # blast data for blastdbcmd on genome for nucleotides of pseudogene
my @blastdbcmdprot; # blast data for blastdbcmd on proteome for protein sequence for paralog of pseudogene
my @blastdbcmdprot2;
my $i=1;
my $v=1;
my @exonerate;
my $newstart;
my $newend;

open (IN, "noIGC.pseudo.loc") || die "nope $! \n";

while ($list = <IN>) { 		# reads in the filed with pseudogenes 

	@list=split(/\t/, $list);
	
	$pacid{$i}=$list[0]; 	# trop pacid
	$query{$i}=$list[1]; 	# Laevis Scaffold where pseudogene is
	$start{$i}=$list[2]; 	# starting location of region where pseudogene would be (end of previous gene)
	$stop{$i}=$list[3];  	# ending location of region where pseudogene would be (beginning of next gene)
	$i++;
}
  
close (IN);
while ($v < $i)  { 			#for each line in the array

	$newstart = ($start{$v}); 
	$newend = ($stop{$v});
	print ("$pacid{$v} $query{$v} $newstart $newend ");

# pseudogene cds	
	push (@blastdbcmdnt, `blastdbcmd -db ~/Laevis/Genome/LAEVIS_7.1.repeatMasked.fa -dbtype nucl -entry $query{$v} -range $newstart-$newend -out "./Data/Laevis7/$pacid{$v}.nt"`);
	#runs blastdcmd on Laevis genome finding location of pseudogene sequence
	
	push (@blastdbcmdprot, `blastdbcmd -db ~/Sahar_safe/Data/Xtr_72_longest_pep.fa -dbtype prot -entry 'lcl|$pacid{$v}' > "./Data/Laevis7/$pacid{$v}.prot"`);
	#runs blastdcmd on Tropicalis proteome finding protein sequence for the paralog of pseudogene

	push (@exonerate, `exonerate --showalignment False --showvulgar False -S False -m protein2genome --ryo ">%ti (%tcb - %tce)\n%tcs\n" ./Data/Laevis7/$pacid{$v}.prot ./Data/Laevis7/$pacid{$v}.nt > ./Data/Laevisfull7/$pacid{$v}`);
	# pseudogene sequence in Laevis to Tropicalis protein --> prints cds of pseudogene
	# --ryo ">%ti (%tcb - %tce)\n%tcs\n"   

# tropicalis cds	
	push (@blastdbcmdprot2, `blastdbcmd -db ~/Sahar_safe/Data/Xtr72.CDS.fa -dbtype nucl -entry 'lcl|$pacid{$v}' >> "./Data/Laevisfull7/$pacid{$v}"`);
	#prints cds of Tropicalis gene
	
#Laeviscds	
#	push (@exonerate, `exonerate --showalignment False --showvulgar False -S False -m protein2genome --ryo ">%ti (%tcb - %tce)\n%tcs\n" ./Data/LaevisA/$pacid{$v}.prot ./Data/LaevisA/$pacid{$v}.nt >> ./Data/Laevisfull7/$pacid{$v}`);
	#prints cds of Laevis existing gene (run LaevisAexonerate2.pl before this)
	# cds= dna sequence that aligns to protein w/o splice sites or introns or frameshifts. 
	
	$v++;
}

