#!/usr/bin/env perl

use strict;
use warnings;

my $file2;
my @trimest;
my $a;


opendir (DH, "./cdsproteinsL") or die "Cannot open: $! \n";
#file input is from blastdbmcd that found Laevis protein.
#perl script: cdsfiles.pl
#takes out polyA tail from Laevis cds and adds Tropicalis CDS onto Laevis file. 

while ($file2 =readdir DH) {
	print ("$file2 ");
	if ($file2 =~ m/d+/) {
		push (@trimest, `trimest -minlength 6 ~/Sahar_work/cdsproteinsL/$file2 ~/Sahar_work/cdsproteins/$file2`);
		#takes out polyA tail from Laevis sequence 6A's in a row, 
		
		open (FILETO, ">> ./cdsproteins/$file2") || die "nope1: $!\n";
		#opens the file that has the Laevis sequence with polyA tail removed. 
		
		open (FILE3, "./cdsproteinsT/$file2") || die "nope2: $! \n";	
		#opens the file that has the protein sequence for Tropicalis ortholog
		
		while(defined($a=<FILE3>)) {
			print FILETO $a; #adds Tropicalis ortholog onto the file with Laevis.
		}
		close (FILE3);
		close (FILETO);
	}
}

closedir(DH);