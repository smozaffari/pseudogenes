#!/usr/bin/env perl

use strict;
use warnings;
my $line;
my @line;
my @files;
my $file;
my $name;

#open (ONE, "fastamuscle") or die "Cannot open: $! \n";
#open (OUT, ">fastaoutput") or die "nope\n";

opendir(DH, "./cdsproteins") or die "Cannot open: $! \n";

while ($file=readdir DH) {
	open (FILEH, "./cdsproteins/$file") or die "nope: $! \n";
	open (OUT, ">./fasta4muscle/$file.fasta") or die "nope: $! \n";
	
	
    # skip . and ..
#    next if($file =~ /^\.$/);
 #   next if($file =~ /^\.\.$/);

    # $file is the file used on this iteration of the loop
	while ($line = <FILEH>) { #read one line at a time
		$line =~ /Command/ && next; #if it has a # go on to next line (common with gff files)
		$line =~ /%tcs/ && next;
		$line =~ / False/ && next;
		$line =~ /Hostname/ && next;
		$line =~ /completed/ &&next;
		$line =~ s/\:/\|/;
	#	@line=split(/\t/, $line);
		if ($line =~ (/\>/)) {
			if ($line =~ /^\d{8}/) {
				$name = $line;
				print ("$name $file\n");
			}
			if ($line =~ /\d{8}/) {
				chomp $name;
				$name =~ s/ //;
				
				print OUT ("$name","_pseudo\n");
				next;
			}
		}
		$line =~ /Scaffold/ &&next;
		print OUT ($line);
	}
	close (FILEH);
	close (OUT);
}
closedir (DH);


