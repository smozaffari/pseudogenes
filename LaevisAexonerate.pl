#!/usr/bin/env perl

use strict;
use warnings;

my $line;
my @line;
my @id;
my %exonstart;
my %exonstop;
my %exon;
my @list;
my $list;
my %pacid;
my %query;
my %start;
my %stop;
my @blastdbcmdnt; #blast data for blastdbcmd on genome for nucleotides of pseudogene
my @blastdbcmdprot; #blast data for blastdbcmd on proteome for protein sequence for paralog of pseudogene
my $i=1;
my $v=1;
my @exonerate;
my $newstart;
my $newend;
my %Laevisname;
my @two;
my $Laevis;
my $var;
my @temp;
my $pseudo;
my @pseudo;
my %pacids;
my $temp2;

open (IN, "LaevisAgene") || die "nope\n";
while ($list = <IN>) { 		#reads in the filed with pseudogenes, sorted and added 500 bp on either side / on one scaffold. 
	@list=split(/\t/, $list);
	
	$query{$i}=$list[0];
	$start{$i}=$list[2];
	chomp $list[3];
	$stop{$i}=$list[3];
	
	$Laevisname{$i}= $list[1];
	$i++;
}

close (IN);

open (FILE, "Besthit.info") || die "nope1 $! \n"; 
while ($line = <FILE>) { 
	@line=split(/\t/, $line); #get Laevis gene name
	chomp $line[1];
	my @two = split(/\|/, $line[0]);
 	$Laevis= (join "\|", $two[0], $two[1]);
	$pacid{$Laevis}=($line[1]);
	# also saved by Tropicalis pacid (key to hash)
	
	print ("$Laevis \t $pacid{$Laevis}\n"); #prints so we know it's working
}
close (FILE);

open (FILE2, "Tropinfo.candidates") || die "nope2 $!\n";
while ($pseudo = <FILE2>) { 
	@pseudo=split(/\t/, $pseudo); #get Laevis gene name
	print "$pseudo[0]\t";
	$pacids{$pseudo[0]} = 1;
}
close (FILE2);
	
while ($v < $i)  { 		#for each line in the array
 #   $pacid = $list[0]; 		#the first column is the pacid that the pseudogene is expected to align most closely to
  #  $query = $list[1]; 		#the second column is the Scaffold on which that pseudogene can be found
   # $start = $list[4]; 		#the location 500bp before where the pseudogene begins aligning to its paralog
   # $stop = $list[5];  		#the location 500bp after where the pseudogene ends aligning to its paralog
	$var = $Laevisname{$v};
	if ($pacid{$var}) {
		$temp2 = ($pacid{$var});
		if ($pacids{$temp2}) {
	
	#	print ("$pacid $query $start");
		
			print ("$var $query{$v} $start{$v} $stop{$v} $pacid{$var} \n");
		  # ($query, $start, $stop) = split(/\t/, $v);
		  # push (@blastdbcmd, `blastdbcmd -dbtype nucl -db /house/homedirs/a/amsession/Xenopus/full_repeats.fa -entry $query`);
		
			$newstart = ($start{$v} -500);
			$newend = ($stop{$v} +500);
	
			push (@blastdbcmdnt, `blastdbcmd -db ~/Laevis/Genome/LAEVIS_6.1.repeatMasked.fa -dbtype nucl -entry $query{$v} -range $newstart-$newend -out "./LaevisA/$pacid{$var}.nt"`);
			#runs blastdcmd on Laevis genome finding location 500 bp +- where pseudogene is found
			
			push (@blastdbcmdprot, `blastdbcmd -db ~/Sahar_safe/Data/Xtr_72_longest_pep.fa -dbtype prot -entry 'lcl|$pacid{$var}' -out "./LaevisA/$pacid{$var}.prot"`);
			#runs blastdcmd on Tropicalis proteome finding protein sequence for the paralog of pseudogene
		
			push (@exonerate, `exonerate --exhaustive y --showalignment True --showvulgar False -S False -m protein2genome:bestfit ./LaevisA/$pacid{$var}.prot ./LaevisA/$pacid{$var}.nt > ./LaevisAexonerate2/$pacid{$var}`);
			# --ryo ">%ti (%tcb - %tce)\n%tcs\n"   
		}
			# to get the dna sequence that aligns to protein w/o splice sites or introns or frameshifts. 
	}
		#CHANGED: to show vulgar output to parse for insertions/deletions
		$v++;
	
}

foreach my $i (@blastdbcmdnt) {
    print "$i\n";
}
