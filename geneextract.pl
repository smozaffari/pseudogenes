#!/usr/bin/env perl

use strict;
use warnings;
my $pacID;
my $count=0;
my $min =-1;
my $max =-1;
my %lx;
my $line;
my @line;
my @id;
my %start;
my %stop;
my @pac;
my $pacid;

#my $numargs = @ARGV; # gff file with gene and mrna, file to write to
#if ($numargs != 2) { 						# makes sure 2 files were used as arguments
#    print "\not enough files\n";			
#    exit;									
#}

open (LSCAFFOLD, "./XB_XENLA_2012oct_longest.gene.gff3 ") or die "Cannot open: $! \n";
open (LXSUM, ">LaevisAgene") or die "$! \n";
while ($line = <LSCAFFOLD>) { #read one line at a time
	$line =~ /#/ && next; #if it has a # go on to next line (common with gff files)
	@line=split(/\t/, $line); # split each word by tabs of each line into an element of an array

	if ($line[2]=~ m/gene/) { #if it is mRNA
		my $name = $line[8];
		@id =split(/\=/, $name);
		@pac = split (/\|/, $id[1]);
		$pacid= join('|', $pac[0], $pac[1]);
		print ("$pacid \t"); #take out the pacid number
		($start{$id[1]}) = $line[3]; #record start by pacID number
		($stop{$id[1]})= $line[4]; #record stop by pacID number
		print LXSUM ("$line[0]\t$pacid\t$start{$id[1]}\t$stop{$id[1]}\n");
	}

}
close(LSCAFFOLD);
close(LXSUM);
