use warnings;
open (IN, "$ARGV[0]") || die "nope\n";
while (<IN>) {
    chomp;
    push (@list, "$_");
}
close (IN);
foreach $file (@list) {
    push (@blastdbcmd, `blastdbcmd -dbtype nucl -db ./Data/Xtr72.CDS.fa -entry 'lcl|$file' | sed 's/lcl|//' > ./R/conserved/"$file".fasta`);
    push (@LA, `perl get_Exonerate_CDS.pl ./Data/LaevisexonerateOne/$file conserved >> ./R/conserved/"$file".fasta`);
    push (@LB, `perl get_Exonerate_CDS.pl ./Data/LaevisexonerateTwo/$file pseudo >> ./R/conserved/"$file".fasta`);
}
